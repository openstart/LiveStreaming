/*
 *
 * To be able to run ffmpeg without it getting stuck in a process we have to handle its input and output in a seperate THREADED class!
   Annoying
 */
package recording_merger;

import java.io.*;
import java.lang.Thread;
import java.lang.Throwable;

/**
 *
 * Reference http://lists.mplayerhq.hu/pipermail/ffmpeg-user/2006-December/005544.html
 */
 

 class ThreadedTranscoderIO extends Thread {

	 InputStream input_;

	 ThreadedTranscoderIO(InputStream input, String name) {
		 super(name);
		 input_ = input;
	 }

	 public void run() {
		 try {
			 int c;
			 while ((c = input_.read()) != -1) {
				 System.out.write(c);
			 }
		 } catch (Throwable t) {
			 t.printStackTrace();
		 }
	 }

 }
