/*
 * UseTranscoderBlocking.java
 *
 * This uses the super awesome application FFMPEG in order to merge two flv's into a final flv
 * REFERNCE: http://www.linglom.com/2007/06/06/how-to-run-command-line-or-execute-external-application-from-java/
 *
 */
package recording_merger;

import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

// For Red5 Logging
import org.slf4j.Logger;
import org.red5.logging.Red5LoggerFactory;

// We need this for threads - this is for the neat line up in a row processing functionality
import java.lang.Thread; 

// For Loading the .properties configuration file
import java.util.Properties;

public class UseTranscoderBlocking {
    
	 // DATAMEMBERS
	    // Get the logger going - this allows us to write logs to red5root/log/recording_merger.log
		//
		private static Logger log = Red5LoggerFactory.getLogger(UseTranscoderBlocking.class, "recording_merger");
		
		// CONFIG FILE
		private static Properties configLoader;
		
		private String FFMPEGFULLPATH = "";
		
		// The relative path from the Red5 root to the applications "sreams" directory
		private String RELATIVEVIDPATH = "\\dist\\webapps\\recording_merger\\streams\\";
		
		// For blocking mode of this class, we return the absolute path to the final encoded file
		// which then gets sent back to the flash player client!
		private String FINALFILEPATH = "";
		
	    public UseTranscoderBlocking() {

	    }
	   
	// ## NAME: Start_FFMPEG
	// ## DESCRIPTION: Attempts to run ffmpeg with certain parameters depending on what is passed to this function.
	// ## Here are example parameters for ffmpeg that should be close to our final parameters:
	// ## ffmpeg -y -i RecordTest_2953_Video.flv -i RecordTest_2953_Audio.flv -sameq RecordTest_2953_Merged.flv
	// ##
	// ## PRECONDITIONS: We must have already recorded the input flv file and on the Flash client, its buffer must have emptied.  Otherwise we will
	// ## be unable to access the file. We need the index of the overlay image
	// ## POSTCONDITIONS: Should dump out a new file with the overlay 
       public String Start_FFMPEG(String _BaseFileName) {
		   String result = "";
			
				try {
					// The config .properties file is setup to reside in the root Red5 directory...thats what will resolve here.
					// Would like to not have it in the red5 root, but if I put it in dist/webapps/..blah blah then thatll probably
					// change later as red5 progresses.
					configLoader = new Properties();
					configLoader.load(new FileInputStream( "recording_merger.properties" ));
					
					// Populate all the global vars
					FFMPEGFULLPATH = configLoader.getProperty("FFMPEGAbsPath");
					RELATIVEVIDPATH = configLoader.getProperty("StreamsDirRelativePath");

					// Get rid of the properties
					configLoader = null;
					
					// Run FFMPEG
					Run_FFMPEG(_BaseFileName);
					
				} catch (Exception e) {
					System.out.println("Start_FFMPEG exception-" + e);
					log.error("Start_FFMPEG-" + e);
				}
				
			result = FINALFILEPATH;
		   return result;
	   }
	   
	// ## NAME: Build_FFMPEG_Video_Merge_Command
	// ## DESCRIPTION: This will build the ffmpeg command based on the config file 
	// ## ffmpeg -y -sameq -i RecordTest_2953_Video.flv -i RecordTest_2953_Audio.flv RecordTest_2953_Merged.flv
	// ##
	// ## PRECONDITIONS: 
	// ## POSTCONDITIONS: 
       private String Build_FFMPEG_Video_Merge_Command(String _RelativePathToVideoFile) {
		   String returnString = "";
		   
		   try {
			// Get the root Red5 Path
			File tmpDir = new File (".");
				
			// Prepare the full path to the video file
			String FullInputVideoPath = tmpDir.getCanonicalPath() + RELATIVEVIDPATH + _RelativePathToVideoFile + "_Video.flv";
			String FullInputAudioPath = tmpDir.getCanonicalPath() + RELATIVEVIDPATH + _RelativePathToVideoFile + "_Audio.flv";
			
			// Prepare the full path to the output video file - from the root of Red5
			String FullOutputVideoPath = tmpDir.getCanonicalPath() + RELATIVEVIDPATH + _RelativePathToVideoFile + "_Merged.flv";

			// Set the final files absolute path
			FINALFILEPATH = FullOutputVideoPath;

		   // I am going to manually reconstruct this.  You could include it in the external config file if you wanted
		   returnString = FFMPEGFULLPATH + " -y -i " + FullInputVideoPath + " -i " + FullInputAudioPath + " " + FullOutputVideoPath;
		   
		   } catch (IOException e) {
				System.out.println("UseFFMPEG Build_FFMPEG_Video_Merge_Command-" + e.toString());
				log.error("Build_FFMPEG_Video_Merge_Command-" + e.toString());
		   }
		   
		   return returnString;
	   }
	   
	// ## NAME: Run_FFMPEG
	// ## This class is meant to be called from inside a new thread created from inside Red5.
	// ## This function will not return until ffmpeg encoding finishes, which is why its imperitive for it to run inside another thread or it will halt red5 execution!
	// ##
	// ## DESCRIPTION: Attempts to run the command ffmpeg
	// ## Need to know the full os path to FFMPEG.  This was made on a win machine (as some of the paths indicate), but should be able to be modified for other systems.
	// ##
	// ##
	// ## PRECONDITIONS: Full path to ffmpeg, correct path to the video we want to encode, correct path to the image we want to overlay, full OS path to watermark.dll
	// ## POSTCONDITIONS: Runs ffmpeg in an attempt to transcode the video and put the overlay on it - needs to put the finished video somewhere
       private void Run_FFMPEG(String _RecordingNameWoExtension) {
			try {
				// If you're going to run this on OSX or Linux, then there will be some subtlties here that you'll have to deal with.
				Runtime rt = Runtime.getRuntime();
				
				String cmd = Build_FFMPEG_Video_Merge_Command(_RecordingNameWoExtension);
		
				Process pr = rt.exec(cmd);
		
				ThreadedTranscoderIO errorHandler = new ThreadedTranscoderIO(pr.getErrorStream(), "Error Stream");
				 errorHandler.start();
				 ThreadedTranscoderIO inputHandler = new ThreadedTranscoderIO(pr.getInputStream(), "Output Stream");
				 inputHandler.start();
				 
				 try {
					 pr.waitFor();
				 } catch (InterruptedException e) {
					 throw new IOException("UseTranscoderBlocking - Run_FFMPEG - process interrupted " + e);
				 }
				
			} catch(Exception e) {
				System.out.println("UseTranscoderBlocking Run_FFMPEG-" + e.toString());
				log.error("Run_FFMPEG-" + e.toString());
			}
	   }
}