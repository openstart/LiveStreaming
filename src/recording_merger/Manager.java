package recording_merger;
/**
* This is the entry point for the recording_merger red5 server-side application. It was developed using Red5 RC2 from SVN in July 2011
* Basically the functionality here is to launch a 3rd party transcoding app to merge separateley recorded audio and video files.
*
* Original work to develop this methodology was completed thanks to:
* Boston Productions Inc. www.bostonproductions.com
  
  SPECIAL NOTES
	If you are not familiar with Red5 or multi-threaded Java applications then take care when editing how this works.  There is an important concept in Red5 that all these functions
	may run conncurrently for different client connections!  So you may end up with two instances of ffmpeg running at once, or tons of instances if you arent careful in how you implement
	calls from the Flash client
**/
// JAVA NEWBIE NOTE <---- For packages you dont need imports for any classes in the same location in the same package
// thats why none of the dynamic classes that I create and destroy are imported.

// Non Standard RED- Classes
import java.io.IOException;

// For logging
import org.slf4j.Logger;
import org.red5.logging.Red5LoggerFactory;

import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.api.stream.IServerStream;
import org.red5.server.api.stream.IStreamCapableConnection;

// Function calls on clients
import org.red5.server.api.IConnection;
import org.red5.server.api.Red5;
import org.red5.server.api.scope.IScope;
import org.red5.server.api.service.IServiceCapableConnection; 

// Thread stuff
import java.lang.Thread;
	
public class Manager extends ApplicationAdapter {
	
	// ********  Global Class Objects *********
	   
	// For Red5
	private IScope appScope;

	private IServerStream serverStream;
	
	private static Logger log = Red5LoggerFactory.getLogger(Manager.class, "recording_merger");
	
	public boolean appStart(IScope app) {
		appScope = app;
			/** {@inheritDoc} */

		log.info("recording_merger appStart");
		
		return true;
	}
	
	@Override
	// This gets called whenever a Flash client connects to the server
	//
	public boolean appConnect(IConnection conn, Object[] params) {
		return super.appConnect(conn, params);
	}
	
    @Override
	// This gets called whenever a Flash client disconnects
	//
	//
	public void appDisconnect(IConnection conn) {
	
		if (appScope == conn.getScope() && serverStream != null) {
			//log.info("Closing server stream using appDisconnect");
			serverStream.close();
		}
		
		super.appDisconnect(conn);
	}
    
// Non Red-5 Specific Functions
	
	// ## NAME: initiateTranscoder
	// ##
	// ## DESCRIPTION: This function can be called from the flash application using the Netconnection "call" procedure in order to initiate a call to ffmpeg
	// ## to merge a video and audio recording. On completion, a callback occurs on the Flash Client with the absolute path of the final file!
	// ##
	// ## WARNING: In Newer versions of Red5 the server may close the connection to the client if there is too much idle time.  Depending on how
	// ## long ffmpeg takes, you may have to kick the server by calling a function on it every now and then using Netconnection "call".
	// ##
	// ## PRECONDITIONS: There must be a flv file that has been recorded in the "streams" directory.  The buffer on the flash client must also be empty!
	// ## POSTCONDITIONS: This is supposed to return the absolute path to the final video file
	public void initiateTranscoder(String _RecordedVideoFileName) {
		
		//Create a thread to do the ffmpeg encoding without
		//blocking red5 and preventing its further execution
		//by passing in an instance of our connection we are able
		// to make a callback to the client once the thread finishes!
		IConnection currentConnection = Red5.getConnectionLocal(); 
		Runnable myRunObject = new ExecuteTranscoderWithCallbacks(_RecordedVideoFileName, currentConnection);
		
		Thread actualThread = new Thread(myRunObject);
		actualThread.start();
	}

	// ## NAME: ExecuteTranscoderWithCallbacks
	// ##
	// ## DESCRIPTION: This is a really risky thing to do without screwing up Red5;  What we do here is have an inner class that handles the execution of
	// ## FFMPEG encoding jobs initiated by flash player clients.  The whole reason I put this together is that we need to be able to do callbacks to the
	// ## flash player clients when ffmpeg encoding is finished.  The real challenge is that we cant block on Red5 because it halts execution of the entire
	// ## server.  We have to start encodes in a separate thread.  I'm doing it here and waiting for completion so we can attempt to make a callback
	// ## to the client once the job is completed right before exiting the new thread.
	// ##
	// ## If you are not familiar with multi-threaded programming in Java, be advised that once this ends execution it will be gone.
	// ## You can start multiple instances of this at the same time and the jobs will spread out across multiple cores.  Just be careful to
	// ## not start two at once that access the same file!
	// ##
	// ## In this thread, while ffmpeg does the encoding it stops execution flow inside "run" below until ffmpeg finishes.  Make sure
	// ## to configure ffmpeg properly so it never prompts for input such as. "Overwirte the current file?" thats what its -y parameter is for.
	// ##
	// ## WARNING: If I have programmed this wrong, its possible these threads will not get cleaned up and crash Red5 or the JVM.
	// ##
	// ## PRECONDITIONS: 
	// ## POSTCONDITIONS: This starts an independent thread of execution that will block until ffmpeg finishes its transcoding
	// ##				  the goal is to some how make a callback to the client who initiated the call right before the thread exits.
	// ##				  The thread runs in a completely different scope from everything above, so we have to pass in the connection
	// ##				  or else it will have no clue that it even existed!
		class ExecuteTranscoderWithCallbacks implements Runnable {
			
			private String RecordedFileName = "";
			IConnection currentConnection;
			
		ExecuteTranscoderWithCallbacks(String _RecordedFileName, IConnection _clientConn) {
			RecordedFileName = _RecordedFileName;
			currentConnection = _clientConn;
		}
		
		// In threaded java programming, this run will automatically run
		public void run(){
			try{
				UseTranscoderBlocking ffmpegTranscoder = new UseTranscoderBlocking();
			
				// This will block until the encoding is done!
				String returnStatus = "";
				
				returnStatus = ffmpegTranscoder.Start_FFMPEG(RecordedFileName);
				
				// Try and invoke a callback on the client!
				// IConnection currentConnection = Red5.getConnectionLocal(); 
				if (currentConnection instanceof IServiceCapableConnection) { 
					IServiceCapableConnection sc = (IServiceCapableConnection) currentConnection;
				// Send the entire absolute file system path of the encoded file back to the flash client!
				// Calls a function on the netConnections "client" object called transcodingCallback
					sc.invoke("transcodingCallback", new Object[]{returnStatus}); 
				} 

			}catch (Exception e){
				e.printStackTrace();
			}//end catch
		}//end run
		
	}//end inner class ExecuteFFMPEGWithCallbacks
}
