//package com.joburgess.websocket;
//
//import java.io.IOException;
//import java.nio.ByteBuffer;
//import java.nio.CharBuffer;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.catalina.websocket.MessageInbound;
//import org.apache.catalina.websocket.StreamInbound;
//import org.apache.catalina.websocket.WebSocketServlet;
//
//public class DemoServlet extends WebSocketServlet {  
//    private static final long serialVersionUID = 1L;  
//    private volatile int byteBufSize;  
//    private volatile int charBufSize;  
//  
//    @Override  
//    public void init() throws ServletException {  
//        super.init();  
//        byteBufSize = getInitParameterIntValue("byteBufferMaxSize", 2097152);  
//        charBufSize = getInitParameterIntValue("charBufferMaxSize", 2097152);  
//    }  
//  
//    public int getInitParameterIntValue(String name, int defaultValue) {  
//        String val = this.getInitParameter(name);  
//        int result;  
//        if(null != val) {  
//            try {  
//                result = Integer.parseInt(val);  
//            }catch (Exception x) {  
//                result = defaultValue;  
//            }  
//        } else {  
//            result = defaultValue;  
//        }  
//  
//        return result;  
//    }  
//  
//    // 创建Inbound实例，WebSocketServlet子类必须实现的方法  
//    @Override  
//    protected StreamInbound createWebSocketInbound(String subProtocol,  
//            HttpServletRequest request) {  
//        return new EchoMessageInbound(byteBufSize,charBufSize);  
//    }  
//    // MessageInbound子类，完成收到WebSocket消息后的逻辑处理  
//    private static final class EchoMessageInbound extends MessageInbound {  
//        public EchoMessageInbound(int byteBufferMaxSize, int charBufferMaxSize) {  
//            super();  
//            setByteBufferMaxSize(byteBufferMaxSize);  
//            setCharBufferMaxSize(charBufferMaxSize);  
//        }  
//        //  二进制消息响应  
//        @Override  
//        protected void onBinaryMessage(ByteBuffer message) throws IOException {  
//            getWsOutbound().writeBinaryMessage(message);  
//        }  
//        // 文本消息响应  
//        @Override  
//        protected void onTextMessage(CharBuffer message) throws IOException {  
//            // 将收到的消息发回客户端  
//            getWsOutbound().writeTextMessage(message);  
//        }  
//    }  
//}  