//package com.joburgess.websocket;
//import java.io.UnsupportedEncodingException;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import net.minidev.json.JSONObject;
//import net.minidev.json.JSONValue;
//import net.minidev.json.parser.JSONParser;
//import net.minidev.json.parser.ParseException;
//import org.apache.mina.core.buffer.IoBuffer;
//import org.red5.logging.Red5LoggerFactory;
//import org.red5.net.websocket.WebSocketConnection;
//import org.red5.net.websocket.listener.WebSocketDataListener;
//import org.red5.net.websocket.model.MessageType;
//import org.red5.net.websocket.model.WSMessage;
//import org.slf4j.Logger;
//
//public class WebSocketChatDataListener
//  extends WebSocketDataListener
//{
//  private static final Logger log = Red5LoggerFactory.getLogger(WebSocketChatDataListener.class, "chat");
//  private Set<WebSocketConnection> connections;
//  
//  public WebSocketChatDataListener()
//  {
//    setProtocol("chat");
//    
//    this.connections = new HashSet();
//  }
//  
//  public void onWSConnect(WebSocketConnection conn)
//  {
//    log.info("Connect: {}", conn);
//    if (conn.getHeaders().containsKey("Sec-WebSocket-Protocol"))
//    {
//      String protocol = (String)conn.getHeaders().get("Sec-WebSocket-Protocol");
//      if (protocol.indexOf("chat") != -1) {
//        log.debug("Chat enabled");
//      } else {
//        log.info("Chat is not in the connections protocol list");
//      }
//    }
//    this.connections.add(conn);
//  }
//  
//  public void onWSDisconnect(WebSocketConnection conn)
//  {
//    log.info("Disconnect: {}", conn);
//    this.connections.remove(conn);
//  }
//  
//  public void onWSMessage(WSMessage message)
//  {
//    if (!this.protocol.equals(message.getConnection().getProtocol()))
//    {
//      log.debug("Skipping message due to protocol mismatch");
//      return;
//    }
//    if ((message.getMessageType() == MessageType.PING) || (message.getMessageType() == MessageType.PONG)) {
//      return;
//    }
//    if (message.getMessageType() == MessageType.CLOSE)
//    {
//      message.getConnection().close();
//      return;
//    }
//    String msg = new String(message.getPayload().array()).trim();
//    log.info("onWSMessage: {}\n{}", msg, message.getConnection());
//    if ((msg.indexOf('{') != -1) && (msg.indexOf(':') != -1))
//    {
//      log.info("JSON encoded text message");
//      
//      JSONObject obj = null;
//      JSONParser parser = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
//      try
//      {
//        obj = (JSONObject)parser.parse(msg);
//        log.debug("Parsed - keys: {}\ncontent: {}", obj.keySet(), obj);
//        for (WebSocketConnection conn : this.connections) {
//          try
//          {
//            conn.send(JSONValue.toJSONString(obj));
//          }
//          catch (UnsupportedEncodingException e) {}
//        }
//      }
//      catch (ParseException e)
//      {
//        log.warn("Exception parsing JSON", e);
//      }
//    }
//    else
//    {
//      log.info("Standard text message");
//      for (WebSocketConnection conn : this.connections) {
//        try
//        {
//          conn.send(msg);
//        }
//        catch (UnsupportedEncodingException e) {}
//      }
//    }
//  }
//}