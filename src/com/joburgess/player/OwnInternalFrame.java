package com.joburgess.player;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

public class OwnInternalFrame extends JInternalFrame{

	private static final long serialVersionUID = -4331307839376186657L;
	public OwnInternalFrame(String address) {

		super(address, true, true, true, false);
		JPanel webBrowserPanel = new JPanel(new BorderLayout());
		final JWebBrowser webBrowser = new JWebBrowser();
		final String t=address;
		UIUtils.setPreferredLookAndFeel();
		NativeInterface.open();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				webBrowser.navigate("http://localhost:5080/Fpzb/player/rtmpplayer.html?address="+t+"&app=Fpzb");
//				webBrowser.navigate("http://localhost:5080/Fpzb/player/index.html?address="+t+"&app=Fpzb");
			}
		});
		webBrowser.setBarsVisible(false);
		webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
		webBrowser.setLocation(0, 0);
		add(webBrowserPanel, BorderLayout.CENTER);
		setResizable(false);
//		setSize(600,431);
		
		setBorder(BorderFactory.createEmptyBorder());
		((BasicInternalFrameUI)getUI()).setNorthPane(null);
//		setLocation(x0ffset*frameNo, y0offset*frameNo);
	}
}
