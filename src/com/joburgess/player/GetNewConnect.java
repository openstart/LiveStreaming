package com.joburgess.player;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;
import com.joburgess.operateProperties.PropertiesIO;

public class GetNewConnect extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LiveStreamDao live=new LiveStreamDaoImpl();

	@Override
	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {
		PrintWriter pw=arg1.getWriter();
		StringBuffer allapps=new StringBuffer("[{");
		PropertiesIO pio=new PropertiesIO();
		List<String> aas=pio.getAllKey();
		
		for(int i=0;i<aas.size();i++){
			if(i!=aas.size()-1){
				allapps.append("\""+aas.get(i)+"\":\""+pio.getValueByKey(aas.get(i))+"\",");
			}else{
				allapps.append("\""+aas.get(i)+"\":\""+pio.getValueByKey(aas.get(i))+"\"");
			}
		}
		allapps.append("}]");
		
		pw.println(allapps.toString());
		pw.flush();
		pw.close();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
		
		req.startAsync().setTimeout(100);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");
		
		PrintWriter pw=resp.getWriter();
		StringBuffer allapps=new StringBuffer("[");
		PropertiesIO pio=new PropertiesIO();
		Page page=new Page();
		page.setSqlStartNumber(0);
		page.setOnePageNumber(10);
		List<LiveStream> aas=live.getHistroyLive(page);
		
		for(int i=0;i<aas.size();i++){
			allapps.append("\""+aas.get(i).getLiveStreamName()+"\"");
		}
		allapps.append("]");
		
		pw.println(allapps.toString());
		pw.flush();
		pw.close();
		
	}

}
