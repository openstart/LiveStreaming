package com.joburgess.player;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.joburgess.bean.LiveStreamInfo;
import com.joburgess.utils.JSONResolve;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;

public class InnerWindow extends JFrame implements Runnable{

	private static final long serialVersionUID = 1L;

	static int frameNo=0;//窗口编号
	static final int x0ffset=0,y0offset=0;//窗口位置偏移量
	
	private JDesktopPane jdp=new JDesktopPane();
	
	public InnerWindow() {
		
		jdp.putClientProperty("JDesktopPane.dragMode", "outline");
		this.setContentPane(jdp);
		this.setSize(new Dimension(400, 300));
		this.setTitle("飞鹏直播监视窗口");
		
//		this.setExtendedState(Frame.MAXIMIZED_BOTH);//初始最大化
		super.setExtendedState(Frame.ICONIFIED | getExtendedState());//最小化
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	/**
	 * 添加视频窗口
	 * @param address
	 * @return
	 */
	public synchronized boolean addWindow(String address){
//		super.setExtendedState(Frame.ICONIFIED | getExtendedState());
		if(jdp.getAllFrames().length<6){
			final String temp=address;
			//打开播放器

			try {
				OwnInternalFrame frame=new OwnInternalFrame(temp);
				++frameNo;
//					System.out.println(frameNo);
//						frame.setLocation(611*(getRow(frameNo)-1)+50, 431*(getLine(frameNo)-1));
				frame.setSize(600, 410);
//				frame.setSize(1920, 1080);
				frame.setClosable(true);
				frame.setVisible(true);
				jdp.add(frame);
				frame.setSelected(true);
			} catch (Exception e1) {
				System.out.println("添加视频窗口!");
			}
		}
		publicWindow();
		return false;
	}
	
	/**
	 * 移除视频窗口
	 * @param address
	 * @return
	 */
	public synchronized boolean removeWindow(final String address){
		UIUtils.setPreferredLookAndFeel();
		NativeInterface.open();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JInternalFrame[] jifs=jdp.getAllFrames();
				for(int i=0;i<jifs.length;i++){
					JInternalFrame jif=jifs[i];
					if(address.equals(jif.getTitle())){
						try {
							jif.setClosed(true);
							jif.dispose();
						} catch (PropertyVetoException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				publicWindow();
			}
		});
		return false;
	}
	
	/**
	 * 整理视频窗口
	 * @return
	 */
	public synchronized boolean publicWindow(){
		JInternalFrame[] jifs=jdp.getAllFrames();
		int j=0;
		if(jifs.length==0){
			this.setVisible(false);
//			super.setExtendedState(Frame.ICONIFIED | getExtendedState());//最小化
		}else{
//			this.setExtendedState(Frame.MAXIMIZED_BOTH);//初始最大化
			if(jifs.length<=3){
				this.setSize(600*jifs.length+0,425);
			}else{
				this.setSize(1800,860);
			}
			if(!this.isVisible())
				this.setVisible(true);
		}
			
		for(int i=jifs.length-1;i>=0;i--){
			jifs[i].setLocation(611*(getRow(j+1)-1), 431*(getLine(j+1)-1));
			j++;
		}
		this.setLocationRelativeTo(null);
		return false;
	}
	
	/**
	 * 获取行参数
	 * @param t
	 * @return
	 */
	public int getLine(int t){
		if(t%3<5&&t%3!=0){
			return (int) Math.round(((float)(t/3)+0.5));
		}else{
			return t/3;
		}
	}
	
	/**
	 * 获取列参数
	 * @param t
	 * @return
	 */
	public int getRow(int t){
		int i=0;
		switch(t%3){
			case 1:i=1;break;
			case 2:i=2;break;
			case 0:i=3;break;
		}
		return i;
	}

	@Override
	public void run() {
		int i=0;
		while(i<=6){
			List<LiveStreamInfo> lsis=new JSONResolve().getLiveStreamInfo();
			for (LiveStreamInfo liveStreamInfo : lsis) {
				boolean b=true;//true:存在该视频播放窗口;false:不存在该视频播放窗口
				JInternalFrame[] jfs=this.jdp.getAllFrames();
				for (JInternalFrame jInternalFrame : jfs) {
					if(jInternalFrame.getTitle().equals(liveStreamInfo.getLiveName()))
						b=false;
				}
//				System.out.println(liveStreamInfo.getLiveName()+"   "+liveStreamInfo.getLiveState());
				if("open".equalsIgnoreCase(liveStreamInfo.getLiveState())){

					if(b){
						i++;
						this.addWindow(liveStreamInfo.getLiveName());
					}
					
				}else if("close".equals(liveStreamInfo.getLiveState())){
					if(!b){
						i--;
						this.removeWindow(liveStreamInfo.getLiveName());
					}
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println("run:"+i);
		}
	}
	
	public static void main(String[] args) {
		InnerWindow iw=new InnerWindow();
		Thread t=new Thread(iw);
		t.start();
	}
	
}
