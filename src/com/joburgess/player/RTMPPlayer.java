package com.joburgess.player;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

/**
 * 
 * @author Joburgess
 *
 */
public class RTMPPlayer extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RTMPPlayer(String address) {
		JPanel webBrowserPanel = new JPanel(new BorderLayout());
		final JWebBrowser webBrowser = new JWebBrowser();
		final String t=address;
		UIUtils.setPreferredLookAndFeel();
		NativeInterface.open();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				webBrowser.navigate("http://localhost:5080/Fpzb/player/rtmpplayer.html?address="+t+"&app=Fpzb");
				System.out.println("http://localhost:5080/Fpzb/player/rtmpplayer.html?address="+t+"&app=Fpzb   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			}
		});
		webBrowser.setBarsVisible(false);
		webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
		webBrowser.setLocation(0, 0);
		add(webBrowserPanel, BorderLayout.CENTER);
		
//		this.setTitle("飞鹏直播-"+address);
//		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(611, 431);
//		this.setResizable(false);
//		this.setLocationByPlatform(true);
		this.setVisible(true);
	}
}