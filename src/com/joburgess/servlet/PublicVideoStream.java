package com.joburgess.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.bean.User;

public class PublicVideoStream extends HttpServlet{
	
	private static final String rootPath=System.getProperty("user.dir")+"\\webapps\\Fpzb\\";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User u=(User)req.getSession().getAttribute("user");
		String videoName=req.getParameter("video");
		OutputStream sos=resp.getOutputStream();
		resp.setContentType("video/mp4");
		
		if(u!=null&&u.getRole().equalsIgnoreCase("0")){
			
			File video=new File(rootPath+"\\videos\\"+videoName);
			FileInputStream fis=new FileInputStream(video);
			resp.setContentLength((int) video.length());
			byte[] b=new byte[1024];
			while(fis.read(b)>0){
				sos.write(b);
			}
			sos.close();
			fis.close();
		}else{
			resp.getWriter().print("此操作不被允许!");
		}
	}
	
}
