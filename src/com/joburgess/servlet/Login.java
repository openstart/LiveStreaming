package com.joburgess.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.impl.UserImpl;
import com.joburgess.bean.User;
import com.joburgess.utils.EncryptionDecryption;

//import sun.org.mozilla.javascript.internal.ast.TryStatement;

public class Login extends HttpServlet{
	
	private EncryptionDecryption ed;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ed=new EncryptionDecryption();
			
			User user=new User();
			user.setUsername(req.getParameter("username"));
			user.setPassword(ed.encrypt(req.getParameter("password")));
			
			List<User> u=new UserImpl().checkUser(user);
			if(u!=null&&u.size()==1){
				req.getSession().setAttribute("user",u.get(0));
				resp.getWriter().println("yes");
			}else{
				resp.getWriter().println("no");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
