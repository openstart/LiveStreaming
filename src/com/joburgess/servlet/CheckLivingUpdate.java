package com.joburgess.servlet;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;
import com.joburgess.bean.User;
import com.joburgess.utils.PageUtils;
import com.joburgess.utils.RtmpInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckLivingUpdate
  extends HttpServlet{
  private LiveStreamDao live = new LiveStreamDaoImpl();
  
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException{
    doPost(req, resp);
  }
  
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException{
    req.setCharacterEncoding("utf-8");
    resp.setContentType("text/html;charset=utf-8");
    
    User u = (User)req.getSession().getAttribute("user");
    int pagenow = Integer.parseInt(req.getParameter("pagenow") == null ? "1" : req.getParameter("pagenow"));
    List<LiveStream> lives = new ArrayList<LiveStream>();
    Page page = new Page();
    page.setSqlStartNumber(PageUtils.calsqlStartNumber(pagenow, RtmpInfo.onepagenumber));
    page.setPageNumberOfNow(pagenow);
    page.setOnePageNumber(RtmpInfo.onepagenumber);
    page.setSearchString(req.getParameter("search"));
    
    LiveStream ls = new LiveStream();
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("page", page);
    if ((u != null) && (u.getRole().equalsIgnoreCase("0"))){
      ls.setInRecycle("0");
      ls.setLiveStreamState("0");
    }else{
      ls.setLiveStreamState("0");
      ls.setCanPlay("1");
      ls.setInRecycle("0");
    }
    map.put("liveStream", ls);
    lives = this.live.getLiving(map);
    resp.getWriter().println(lives.size());
  }
}
