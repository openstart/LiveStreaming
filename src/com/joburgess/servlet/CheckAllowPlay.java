package com.joburgess.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.User;

public class CheckAllowPlay extends HttpServlet{

	private LiveStreamDao ls=new LiveStreamDaoImpl();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		
		User u=(User) req.getSession().getAttribute("user");
		LiveStream live=new LiveStream();
		live.setLiveStreamId(req.getParameter("liveid"));
		LiveStream temp=ls.findLiveByid(Integer.valueOf(req.getParameter("liveid")));
		PrintWriter out=resp.getWriter();
		
		try {
			if(u!=null&&u.getRole().equalsIgnoreCase("0")){
				out.print(true);
			}else{
				out.print(ls.checkAllowPlay(live));
			}
		} catch (Exception e) {
			out.print("exception");
		}
	}
}
