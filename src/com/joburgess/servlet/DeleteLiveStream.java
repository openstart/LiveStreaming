package com.joburgess.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.impl.LiveInfoDaoImpl;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.utils.FileUtils;

public class DeleteLiveStream extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		
		String[] liveids=req.getParameter("liveid").split(",");
		String control=req.getParameter("control");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		if(control.equals("restore")){
			if(new LiveStreamDaoImpl().updateOutRecycleById(liveids)){
				resp.getWriter().write("OK");
			}else{
				resp.getWriter().write("NO");
			}
		}else{
			LiveStream live=new LiveStreamDaoImpl().findLiveByid(Integer.parseInt(liveids[0]));
			if(live.getInRecycle().equals("0")){
				if(new LiveStreamDaoImpl().updateinRecycleById(liveids)){
					resp.getWriter().write("OK");
				}else{
					resp.getWriter().write("NO");
				}
			}else{
				if(new LiveStreamDaoImpl().deleteLiveStreamById(liveids)&&new LiveInfoDaoImpl().removeLiveinfoByLiveid(liveids)){
					FileUtils.deleteFile(live.getLiveStreamLocalFileName());
					resp.getWriter().write("OK");
				}else{
					resp.getWriter().write("NO");
				}
			}
		}
		
	}
}
