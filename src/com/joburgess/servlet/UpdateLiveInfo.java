package com.joburgess.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.LiveInfoDAO;
import com.joburgess.Dao.impl.LiveInfoDaoImpl;
import com.joburgess.bean.LiveInfo;
import com.joburgess.bean.User;

public class UpdateLiveInfo extends HttpServlet{
	
	LiveInfoDAO lid=new LiveInfoDaoImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out=resp.getWriter();
		
		String livetittle=req.getParameter("livetittle");
		String livedescribe=req.getParameter("livedescribe");
		String liveid=req.getParameter("liveid");
		
		LiveInfo liveinfo=new LiveInfo();
		liveinfo.setLivetittle(livetittle);
		liveinfo.setLivedescribe(livedescribe);
		liveinfo.setLiveid(Integer.parseInt(liveid));
		
		if(((User)req.getSession().getAttribute("user")).getRole().equalsIgnoreCase("0")){
			if(lid.updateLiveInfoByLiveid(liveinfo)){
				out.print("operate success");
			}else{
				out.println("operate failed");
			}
		}else{
			out.println("don't have permission");
		}
		
	}
	
}
