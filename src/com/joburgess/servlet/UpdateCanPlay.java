package com.joburgess.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.User;

public class UpdateCanPlay extends HttpServlet{
	
	private static LiveStreamDao lsd=new LiveStreamDaoImpl();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");

		LiveStream ls=new LiveStream();
		ls.setLiveStreamName(req.getParameter("name"));
		ls.setLiveStreamLocalFileName(req.getParameter("fileName"));
		ls.setLiveStreamId(req.getParameter("liveid"));
		ls.setCanPlay(req.getParameter("canPlay"));
		
		User u=(User)req.getSession().getAttribute("user");
		if(u!=null&&u.getRole().equals("0")){
			lsd.updateCanPlay(ls);
		}else{
			resp.getWriter().write("非法操作!");
		}
		
	}
	
}	
