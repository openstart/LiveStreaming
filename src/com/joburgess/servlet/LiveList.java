package com.joburgess.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.joburgess.Dao.LiveInfoDAO;
import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveInfoDaoImpl;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveInfo;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;
import com.joburgess.bean.User;
import com.joburgess.utils.PageUtils;
import com.joburgess.utils.RtmpInfo;

public class LiveList extends HttpServlet{
	
	private LiveStreamDao live=new LiveStreamDaoImpl();
	private LiveInfoDAO lid=new LiveInfoDaoImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setContentType("text/html;charset=utf-8");
		
		User u=(User) req.getSession().getAttribute("user");
		String state=req.getParameter("state")==null?"all":req.getParameter("state");
		int pagenow=Integer.parseInt(req.getParameter("pagenow")==null?"1":req.getParameter("pagenow"));
		List<LiveStream> lives=new ArrayList<LiveStream>();
		List<LiveInfo> liveinfos=lid.getAllLiveInfo();
		Page page=new Page();
		page.setSqlStartNumber(PageUtils.calsqlStartNumber(pagenow, RtmpInfo.onepagenumber));
		page.setPageNumberOfNow(pagenow);
		page.setOnePageNumber(RtmpInfo.onepagenumber);
		page.setSearchString(req.getParameter("search"));

		LiveStream ls=new LiveStream();
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("page", page);
		if(u!=null&&u.getRole().equalsIgnoreCase("0")){
			ls.setInRecycle("0");
			if(state.equalsIgnoreCase("1")){
				ls.setLiveStreamState("0");
			}else if(state.equalsIgnoreCase("2")){
				ls.setLiveStreamState("1");
			}else if(state.equalsIgnoreCase("3")){
				ls.setInRecycle("1");
			}
			if(state.equalsIgnoreCase("4")){
				lives=live.search(page);
			}else{
				map.put("liveStream", ls);
				lives=live.getLiving(map);
			}
		}else{
			if(state.equalsIgnoreCase("1")){//直播
				ls.setLiveStreamState("0");
			}else if(state.equalsIgnoreCase("2")){//历史
				ls.setLiveStreamState("1");
			}
			ls.setCanPlay("1");
			ls.setInRecycle("0");
			map.put("liveStream", ls);
			lives=live.getLiving(map);
		}
		page.setNumberOfPage(PageUtils.calPageNumber(live.getLiveNumber(map), RtmpInfo.onepagenumber));
		req.setAttribute("lives", lives);
		req.setAttribute("pageinfo", page);
		req.setAttribute("liveinfos", liveinfos);
		req.getRequestDispatcher("playerlist/liveList.jsp").forward(req, resp);
	}
	
}
