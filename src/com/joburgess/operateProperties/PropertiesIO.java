package com.joburgess.operateProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class PropertiesIO {
	
	private static Properties p=new Properties();
//	private static String filePath=Class.class.getResource("/").getPath()+"\\appInfo"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".properties";
	private static String filePath="D:\\appInfo"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".properties";
	
	static{
		try {
			if(!new File(filePath).exists())
				new File(filePath).createNewFile();
			p.load(new FileInputStream(new File(filePath)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取所有的属性名称
	 * @return
	 */
	public List<String> getAllKey(){
		List<String> allKeys=new ArrayList<String>();
		
		Iterator i=p.stringPropertyNames().iterator();
		
		while(i.hasNext()){
			allKeys.add(i.next().toString());
		}
		
		return allKeys;
	}
	
	/**
	 * 根据属性名获取属性值
	 * @param Key
	 * 		属性名
	 * @return
	 */
	public String getValueByKey(String Key){
		return p.getProperty(Key);
	}
	
	/**
	 * 写入属性
	 * @param key
	 * 		属性名
	 * @param value
	 * 		属性值
	 * @return
	 */
	public boolean writeProperties(String key,String value){
		
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(new File(filePath));
			p.setProperty(key, value);
			p.store(fos, null);
			fos.close();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}

//	public static void main(String[] args) throws InterruptedException {
//		PropertiesIO p=new PropertiesIO();
//		p.writeProperties("job", "123");
//	}
	
}
