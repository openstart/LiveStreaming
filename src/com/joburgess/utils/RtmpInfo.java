package com.joburgess.utils;

import java.net.InetAddress;

public class RtmpInfo {
	
	public static String liveduration="武汉飞鹏数码有限责任公司成立于2004年4月，是获得\"双软认定\"的国家\"高新技术企业\"，公司以\"致力于传媒信息化，丰富受众文化生活\"为使命，积极推进广电行业\"数字化、网络化、信息化\"的发展。";//初始视频描述信息
	
	public static String ffmpegPath=System.getProperty("user.dir")+"\\webapps\\Fpzb\\static\\ffmpeg\\ffmpeg64.exe";//ffmpeg路径
	public static String rtmpUrl=null;//rtmp推流地址
	public static String keys="Joburgess";//默认秘钥
	public static int ffmpegpoolsize=5;//ffmpeg视频转换线程池大小
	public static int screenshottime=2;//直播截图延迟时间
	public static int convertquality=1;//视频转换质量
	public static int minvideolength=3;//视频录制最小长度
	public static int onepagenumber=12;//每页显示数据
	
	static{
		try {
			rtmpUrl="rtmp://"+InetAddress.getLocalHost().getHostAddress()+":1935/Fpzb/";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
