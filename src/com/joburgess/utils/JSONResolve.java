package com.joburgess.utils;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.joburgess.bean.LiveStreamInfo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class JSONResolve {

	public List<LiveStreamInfo> getLiveStreamInfo(){
		List<LiveStreamInfo> lsis=new ArrayList<LiveStreamInfo>();
		
		try {
			URL url=new URL("http://192.168.1.39:5080/Fpzb/gnc");
			URLConnection uc=url.openConnection();
			InputStream fs=uc.getInputStream();
			StringBuffer sb=new StringBuffer();
			byte[] b=new byte[1024];
			while((fs.read(b))>0){
//				System.out.print(new String(b).trim()+"<><><><><><><><>");
				sb.append(new String(b).trim());
			}
			
//			System.out.println(sb.toString()+">>>>>>>>>>>>>>>");
			JSONArray jsonArray=JSONArray.fromObject(sb.toString().substring(0, sb.toString().lastIndexOf("]")+1));
			JSONObject jsonObject=JSONObject.fromObject(jsonArray.get(0).toString());
			for(Object o:jsonObject.keySet()){
				LiveStreamInfo lsi=new LiveStreamInfo();
				lsi.setLiveName(o.toString());
				lsi.setLiveState(jsonObject.get(o).toString());
				lsis.add(lsi);
			}
		} catch (Exception e) {
			System.out.println("数据解析错误！"+e.getMessage());
//			e.printStackTrace();
		}
		
		return lsis;
	}
	
}
