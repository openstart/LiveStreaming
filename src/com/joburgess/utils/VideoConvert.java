package com.joburgess.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.fpzb.Application;

public class VideoConvert implements Runnable{
	
	private static Logger log = Red5LoggerFactory.getLogger(VideoConvert.class, "VideoConvert");
	
	private String ffmpegPath=RtmpInfo.ffmpegPath;
	private String filePath=null;
	private LiveStreamDao liveUtils=new LiveStreamDaoImpl();
	
	public VideoConvert(String filepath) {
		this.filePath=filepath;
	}
	
	/**
	 * 将flv视频转换为MP4视频文件
	 * @param videofile
	 * @return
	 */
	public synchronized boolean toMp4(String videofile){
		
//		if(FileUtils.getVideoLength(System.getProperty("user.dir")+"\\webapps\\Fpzb\\streams\\"+videofile+".flv")>RtmpInfo.minvideolength){
			this.log.info("开始转换...");
			
			Process process=null;
			
			List<String> commands=new ArrayList<String>();
			commands.add(ffmpegPath);
			commands.add("-i");
			commands.add(System.getProperty("user.dir")+"\\webapps\\Fpzb\\streams\\"+videofile+".flv");
//			commands.add(System.getProperty("user.dir")+"\\webapps\\Fpzb\\streams\\"+videofile+".fpzb");
			commands.add("-ab");//音频数据流
			commands.add("64");
			commands.add("-ar");//声音频率
			commands.add("22050");
//			commands.add("-b");
//			commands.add("25k");
//			commands.add("-metadata");//设置视频元数据
//			commands.add("title=whfpsoft");//视频标题
			commands.add("-r");//帧率
			commands.add("30");//30帧/秒
//			commands.add("-s");//输出文件的尺寸
//			commands.add("1080*720");
			commands.add("-qscale");//视频质量
			commands.add(String.valueOf(RtmpInfo.convertquality));//值越小，视频质量越高
//			commands.add("-vf");//添加水印
//			commands.add("\"movie=logo.png [joburgess];[in][joburgess] overlay=10:10\"");//添加水印
			
			commands.add("-vcodec");
			commands.add("copy");
			commands.add("-acodec");
			commands.add("copy");
			commands.add("-y");//覆盖文件
			commands.add(System.getProperty("user.dir")+"\\webapps\\Fpzb\\videos\\"+videofile+".mp4");
			
			String t="";
			for (String string : commands) {
				t+=string+" ";
			}
			System.out.println(t);
			try {
				ProcessBuilder processBuilder=new ProcessBuilder();
				processBuilder.command(commands);
				
				process=processBuilder.start();
				
				DataInputStream errinput = new DataInputStream(process.getErrorStream());
				BufferedReader errResult = new BufferedReader(new InputStreamReader(errinput));

				errResult.close();
				
				process.waitFor();
				process.destroy();
				return true;
			} catch (Exception e) {
				this.log.error("格式转换警告:"+e.getMessage());
				return false;
			}finally{
				if(FileUtils.getVideoLength(System.getProperty("user.dir")+"\\webapps\\Fpzb\\videos\\"+videofile+".mp4")>RtmpInfo.minvideolength){
					this.log.info("转换完成！");
					liveUtils.updateLiveState(videofile);
				}else{
					FileUtils.deleteFile(videofile);
					liveUtils.deleteMinVideo(videofile);
				}
			}
//		}else{
//			new File(System.getProperty("user.dir")+"\\webapps\\Fpzb\\streams\\"+videofile+".flv").delete();
//			return false;
//		}
		
	}

	@Override
	public void run() {
		toMp4(filePath);
	}
	
//	public static void main(String[] args) {
//		VideoConvert vc=new VideoConvert();
//		System.out.println(vc.toMp4("Joburgess(2016.05.26-07.58.35)"));
////		vc.toMp4("Joburgess(2016.05.26-07.39.10)");
////		vc.toMp4("*");
//	}
}
