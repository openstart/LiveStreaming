package com.joburgess.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.InputFormatException;

public class FileUtils {
	
	private static final String rootPath=System.getProperty("user.dir")+"\\webapps\\Fpzb\\";
	
	/**
	 * 移动缓存视频文件
	 * @param fileName
	 * 			被移动文件名
	 * @param fromDir
	 * @param toDir
	 * @return
	 */
	public boolean removeFile(String fileName,String fromDir,String toDir){
		String timeAddr="("+ new SimpleDateFormat("yyyy.MM.dd-hh.mm.ss").format(new Date()) + ")";
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis=new FileInputStream(rootPath+fromDir+"\\"+fileName+".flv");
			fos=new FileOutputStream(rootPath+toDir+"\\"+fileName+timeAddr+ ".flv");
			
			byte[] b=new byte[1024];
			int a=0;
			while((a=fis.read(b))>0){
				fos.write(b, 0, a);
			}
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			try {
				if(fos!=null){
					fos.close();
				}
				if(fis!=null){
					fis.close();
				}
				new File(rootPath+fromDir+"\\"+fileName+".flv").delete();
				//视频转换
//				new VideoConvert().toMp4(fileName+timeAddr);
				new Thread(new VideoConvert(fileName+timeAddr)).start();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * 改名
	 * @param fileName
	 * @param fromDir
	 * @return
	 */
	public boolean renameFile(String fileName,String fromDir){
		String timeAddr="("+ new SimpleDateFormat("yyyy.MM.dd-hh.mm.ss").format(new Date()) + ")";
		try {
			new File(rootPath+fromDir+"\\"+fileName+".flv").renameTo(new File(rootPath+fromDir+"\\"+fileName+".fpzb"));
			System.out.println(rootPath+fromDir+"\\"+fileName+".fpzb");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally{
			//视频转换
//			new VideoConvert().toMp4(fileName);
			new Thread(new VideoConvert(fileName)).start();
		}
	}
	
	/**
	 * 删除文件
	 * @param fileName
	 * @return
	 */
	public static boolean deleteFile(String fileName){
		boolean b=true;
		
		File image=new File(rootPath+"/screenshot/"+fileName+".jpg");
		File mp4=new File(rootPath+"/videos/"+fileName+".mp4");
		File flv=new File(rootPath+"/streams/"+fileName+".flv");
		
		if(image.exists()){
			b=image.delete();
		}else{
			System.out.println("图片文件不存在！");
		}
		if(mp4.exists()){
			b=mp4.delete();
		}else{
			System.out.println("mp4文件不存在！");
		}
		if(flv.exists()){
			b=flv.delete();
		}else{
			System.out.println("flv文件不存在！");
		}
		return b;
	}
	
	/**
	 * 获取视频长度
	 * @param videopath
	 * @return
	 */
	public static long getVideoLength(String videopath){
		try {
//			Encoder encoder=new Encoder();
			return new Encoder().getInfo(new File(videopath)).getDuration()/(long)1000;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}
