package com.joburgess.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.red5.logging.Red5LoggerFactory;
import org.slf4j.Logger;

public class VideoStreamUtils implements Runnable{

	private Logger log=Red5LoggerFactory.getLogger(this.getClass(), "VideoStreamUtils");
	private static final String url=RtmpInfo.rtmpUrl;
	private String ffmpegPath=RtmpInfo.ffmpegPath;
	private String videoStreamName=null;
	private String screenShotName=null;
	
	public VideoStreamUtils(String vsn,String ssn) {
		this.videoStreamName=vsn;
		this.screenShotName=ssn;
	}

	public synchronized void StreamShot(){
		
		log.info("直播截图...");
		System.out.println("\""+url+videoStreamName+" live=1\"");
		
		List<String> commands=new ArrayList<String>();
		
		commands.add(ffmpegPath);
		commands.add("-probesize");
		commands.add("32768");
		commands.add("-i");
		commands.add("\""+url+videoStreamName+" live=1\"");
		commands.add("-ss");
		commands.add(String.valueOf(RtmpInfo.screenshottime));
		commands.add("-f");
		commands.add("image2");
		commands.add("-r");
		commands.add("1");
		commands.add(System.getProperty("user.dir")+"\\webapps\\Fpzb\\screenshot\\"+screenShotName+".jpg");
		
		try {
			ProcessBuilder command=new ProcessBuilder(commands);
			Process process=command.start();
			DataInputStream errinput = new DataInputStream(process.getErrorStream());
			BufferedReader errResult = new BufferedReader(new InputStreamReader(errinput));

//			while ((errResult.readLine()) != null) {
//				System.out.println("errResult:" + errResult.readLine());
//			}
			errResult.close();
			
			process.waitFor();
			process.destroy();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		StreamShot();
	}
	
}


