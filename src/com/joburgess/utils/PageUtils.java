package com.joburgess.utils;

public class PageUtils {

	/**
	 * 计算页数
	 * @param number
	 * 		数据总数
	 * @param pagenumber
	 * 		每页条数
	 * @return
	 */
	public static int calPageNumber(int number,int pagenumber){
		int i=number/pagenumber;
		int t=number%pagenumber;
		if(t>0){
			return ++i;
		}else{
			return i;
		}
	}
	
	/**
	 * 计算起始数据索引值
	 * @param pagenownumber
	 * @param pagenumber
	 * @return
	 */
	public static int calsqlStartNumber(int pagenownumber,int pagenumber){
		return pagenumber*(pagenownumber-1);
	}
}
