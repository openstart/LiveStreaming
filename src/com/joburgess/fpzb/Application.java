package com.joburgess.fpzb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.red5.logging.Red5LoggerFactory;
import org.red5.net.websocket.WebSocketPlugin;
import org.red5.net.websocket.WebSocketScopeManager;
import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.adapter.MultiThreadedApplicationAdapter;
import org.red5.server.api.IClient;
import org.red5.server.api.IConnection;
import org.red5.server.api.Red5;
import org.red5.server.api.scope.IScope;
import org.red5.server.api.service.IPendingServiceCall;
import org.red5.server.api.service.IPendingServiceCallback;
import org.red5.server.api.service.IServiceCapableConnection;
import org.red5.server.api.stream.IBroadcastStream;
import org.red5.server.api.stream.IPlayItem;
import org.red5.server.api.stream.IServerStream;
import org.red5.server.api.stream.IStreamAwareScopeHandler;
import org.red5.server.api.stream.ISubscriberStream;
import org.red5.server.plugin.PluginRegistry;
import org.red5.server.stream.ClientBroadcastStream;
import org.red5.service.httpstream.SegmenterService;
import org.slf4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.joburgess.Dao.LiveInfoDAO;
import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveInfoDaoImpl;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveInfo;
import com.joburgess.bean.LiveStream;
import com.joburgess.operateProperties.PropertiesIO;
import com.joburgess.utils.RtmpInfo;
import com.joburgess.utils.VideoConvert;
import com.joburgess.utils.VideoStreamUtils;

//public class Application extends ApplicationAdapter implements IPendingServiceCallback,IStreamAwareScopeHandler,ApplicationContextAware{
public class Application extends MultiThreadedApplicationAdapter
		implements IPendingServiceCallback, IStreamAwareScopeHandler, ApplicationContextAware {
	private IScope appScope;
	private IServerStream serverStream;

	List<String> clients;
	// HashSet<String> clientname=new HashSet<String>();
	List<String> clientname = new ArrayList<String>();
	// private PropertiesIO pio=new PropertiesIO();
	private static Logger log = Red5LoggerFactory.getLogger(Application.class, "Fpzb");
	private ApplicationContext applicationContext;

	// create a instance for application
	public Application app;
	public IConnection conn;
	public IScope scope;
	public ClientBroadcastStream stream;// 用来接受flash上传的stream的类

	private LiveStreamDao liveUtils = new LiveStreamDaoImpl();
	private LiveStream live = null;
	private LiveInfoDAO liveinfod = new LiveInfoDaoImpl();
	private LiveInfo liveinfo = null;

	private ExecutorService ffmpegpool = Executors.newFixedThreadPool(RtmpInfo.ffmpegpoolsize);// 限制为5的线程池

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("\r\n-----------------------setApplicationContext-----------------------\r\n");
		this.applicationContext = applicationContext;
	}

	/**
	 * 服务开启
	 */
	public boolean appStart(IScope app) {
		System.out.println("\r\n-----------------------appStart-----------------------\r\n");
		
		//开启websocket服务
//		WebSocketScopeManager manager = ((WebSocketPlugin) PluginRegistry.getPlugin("WebSocketPlugin")).getManager();
//		manager.addApplication(app);

		super.appStart(app);
		this.log.info("飞鹏直播服务器已启动;本机推流地址：" + RtmpInfo.rtmpUrl);
		this.appScope = app;
		return true;
	}

	@Override
	public boolean appJoin(IClient arg0, IScope arg1) {
		System.out.println("\r\n-----------------------AppJoin-----------------------\r\n");

		return super.appJoin(arg0, arg1);
	}

	public boolean appConnect(IConnection conn, Object[] params) {
		System.out.println("\r\n-----------------------appConnect-----------------------\r\n");

		clients = new ArrayList<String>();
		this.conn = conn;
		this.log.info(conn.getRemoteAddress() + "已连接到服务器!");
		IScope appScope = conn.getScope();
		this.log.debug("App connect called for scope: {}", appScope.getName());

		Map<String, Object> properties = conn.getConnectParams();
		if (this.log.isDebugEnabled()) {
			for (Map.Entry<String, Object> e : properties.entrySet()) {
				this.log.debug("连接属性: {} = {}", e.getKey(), e.getValue());
			}
		}

		return super.appConnect(conn, params);
	}

	public void appDisconnect(IConnection conn) {
		System.out.println("\r\n-----------------------appDisconnect-----------------------\r\n");

		if ((this.appScope == conn.getScope()) && (this.serverStream != null)) {
			this.serverStream.close();
		}
		super.appDisconnect(conn);
		// this.closeClient();
		// callClient("Open");
	}

	@Override
	public void streamBroadcastStart(IBroadcastStream stream) {
		System.out.println("\r\n-----------------------streamBroadcastStart-----------------------\r\n");

		String fileName = stream.getPublishedName() + "(" + new SimpleDateFormat("yyyyMMddhhmmss").format(new Date())
				+ ")";

		live = new LiveStream();
		liveinfo = new LiveInfo();
		live.setLiveStreamName(stream.getPublishedName());
		live.setLiveStreamLocalFileName(fileName);
		live.setLiveStreamShotFileName(fileName);
		liveinfo.setLivetittle(stream.getPublishedName());
		liveinfo.setLivedescribe(RtmpInfo.liveduration);
		liveinfo.setDuration(0);

//		new PropertiesIO().writeProperties(stream.getPublishedName(), "open");

		// clientname.add(stream.getPublishedName());
		// callClient(stream.getPublishedName()+"Open");

		try {

			// 保存直播流
			stream.saveAs(fileName, false);// 储存为flv

			// 直播截图
			ffmpegpool.execute(new VideoStreamUtils(stream.getPublishedName(), fileName));
			this.log.info("开始保存直播数据流：" + fileName);
		} catch (Exception e) {
			this.log.info("数据存储错误!");
		}
		liveUtils.addLiveStream(live);
		liveinfo.setLiveid(Integer
				.parseInt(liveUtils.getLiveStreamByStreamNameAndState(stream.getPublishedName()).getLiveStreamId()));
		liveinfod.addLiveInfo(liveinfo);
		this.log.info(stream.getPublishedName() + "直播已成功建立!");
		super.streamBroadcastStart(stream);
		this.getAllClient();
	}

	@Override
	public void streamPublishStart(final IBroadcastStream stream) {
		System.out.println("\r\n-----------------------streamPublishStart-----------------------\r\n");
		final String streamName = stream.getPublishedName();
		log.debug("streamPublishStart - stream name: {}", streamName);
		IConnection conn = Red5.getConnectionLocal();

		conn.setAttribute("streamName", streamName);
		super.streamPublishStart(stream);
		if ((stream instanceof ClientBroadcastStream)) {
			Thread creator = new Thread(new Runnable() {
				public void run() {
					while (Application.this.scope.getBroadcastScope(streamName) == null) {
						Application.log.debug("Stream: {} is not available yet...", streamName);
						try {
							Thread.sleep(500L);
						} catch (InterruptedException e) {
						}
					}
					SegmenterService segmenter = (SegmenterService) Application.this.applicationContext
							.getBean("segmenter.service");
					if (!segmenter.isAvailable(streamName)) {
						segmenter.start(Application.this.scope, stream, true);
					}
				}
			});
			creator.setDaemon(true);
			creator.start();
		}
	}

	@Override
	public IBroadcastStream getBroadcastStream(IScope arg0, String arg1) {
		System.out.println("\r\n-----------------------getBroadcastStream-----------------------\r\n");
		return super.getBroadcastStream(arg0, arg1);
	}

	@Override
	public void streamBroadcastClose(IBroadcastStream arg0) {
		System.out.println("\r\n-----------------------streamBroadcastClose-----------------------\r\n");

		try {

			super.streamBroadcastClose(arg0);
//			new PropertiesIO().writeProperties(arg0.getPublishedName(), "close");
			this.log.info(arg0.getPublishedName() + "直播结束！");

			this.getAllClient();
		} catch (Exception e) {
			this.log.error(arg0 + "livestreaming is not exit");
		} finally {
			// 视频格式转换
			ffmpegpool.execute(new VideoConvert(
					liveUtils.getLiveStreamByStreamNameAndState(arg0.getPublishedName()).getLiveStreamLocalFileName()));
			liveUtils.updateLiveStateToConverting(arg0.getPublishedName());
		}
	}

	@Override
	public void resultReceived(IPendingServiceCall arg0) {
		System.out.println("\r\n-----------------------resultReceived-----------------------\r\n");

		this.log.info("客户端返回值：" + arg0.getResult());
	}

	@Override
	public boolean roomStart(IScope arg0) {
		System.out.println("\r\n-----------------------roomStart-----------------------\r\n");

		this.log.info("启动room");
		return super.roomStart(arg0);
	}

	@Override
	public boolean roomConnect(IConnection arg0, Object[] arg1) {
		System.out.println("\r\n-----------------------roomConnect-----------------------\r\n");
		IScope iscopes = arg0.getScope();
		System.out.println("连接到" + iscopes.getName() + "ID列表");
		Set<IClient> i = iscopes.getClients();
		for (IClient iClient : i) {
			System.out.println(iClient.getId() + "....................");
		}
		return super.roomConnect(arg0, arg1);
	}

	/**
	 * 调用客户端getInfo方法
	 * 
	 * @param info
	 */
	public void callClient(String info) {
		try {
			IConnection con = Red5.getConnectionLocal();
			Iterator<Set<IConnection>> allclient = con.getScope().getConnections().iterator();
			while (allclient.hasNext()) {
				Iterator<IConnection> it = allclient.next().iterator();
				while (it.hasNext()) {
					IServiceCapableConnection iscc = (IServiceCapableConnection) it.next();
					iscc.invoke("getInfo", new Object[] { info }, this);
					System.out.println(info + "------------------------------");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * 获取当前所有直播名称
	 */
	public List<String> getAllClient() {
		List<String> allStreams = new ArrayList<String>();

		IConnection con = Red5.getConnectionLocal();
		Iterator<String> t = con.getScope().getScopeNames().iterator();

		StringBuffer temp = new StringBuffer();
		int i = 0;
		while (t.hasNext()) {
			i++;
			String temp1 = t.next();
			allStreams.add(temp1);
			temp.append(temp1 + "\r\n");
		}

		System.out.println("当前直播用户:" + i + "位");
		System.out.println(temp);
		return allStreams;
	}

	@Override
	public void appStop(IScope arg0) {
		System.out.println("\r\n-----------------------appStop-----------------------\r\n");

		//关闭websocket服务
//		WebSocketScopeManager manager = ((WebSocketPlugin) PluginRegistry.getPlugin("WebSocketPlugin")).getManager();
//		manager.removeApplication(arg0);

		ffmpegpool.shutdown();
		super.appStop(arg0);
	}

	public void closeClient() {
		System.out.println("关闭中...");
		IConnection conn = Red5.getConnectionLocal();
		Iterator<IConnection> t = conn.getScope().getClientConnections().iterator();
		while (t.hasNext()) {
			IConnection iconn = t.next();
			System.out.print(iconn.getRemoteAddress() + " >>> ");
			Iterator<String> it = iconn.getScope().getScopeNames().iterator();
			for (int i = 0; it.hasNext(); i++) {
				System.out.println(it.next() + "   " + i);
			}
		}
	}

	@Override
	public void streamPlayItemPause(ISubscriberStream stream, IPlayItem item, int position) {
		System.out.println("\r\n-----------------------streamPlayItemPause-----------------------\r\n");
		// TODO Auto-generated method stub
		super.streamPlayItemPause(stream, item, position);
		this.log.info("停止播放" + item.getName());
		// stream.getConnection().close();
	}

}
