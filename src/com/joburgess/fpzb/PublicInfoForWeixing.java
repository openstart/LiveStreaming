package com.joburgess.fpzb;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PublicInfoForWeixing extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");

		PrintWriter pw=resp.getWriter();
		StringBuffer videoList=new StringBuffer("[");
		
		File videoDir=new File(System.getProperty("user.dir")+"\\webapps\\Fpzb\\streams");
		System.out.println(System.getProperty("user.dir")+"\\videos");
		File[] videos=videoDir.listFiles();
		if(videos.length>0){
			for (int i=0;i<videos.length;i++) {
				if(i!=videos.length-1){
					videoList.append("'"+videos[i].getName()+"',");
				}else{
					videoList.append("'"+videos[i].getName()+"'");
				}
			}
			videoList.append("]");
		}else{
			System.out.println("无视频文件!");
		}
		pw.println(videoList.toString());
		pw.flush();
		pw.close();
	}
	
	
//	public static void main(String[] args) {
//		StringBuffer videoList=new StringBuffer("[");
//		
//		File videoDir=new File(System.getProperty("user.dir")+"\\streams");
//		System.out.println(System.getProperty("user.dir")+"\\streams");
//		File[] videos=videoDir.listFiles();
//		if(videos.length>0){
//			for (int i=0;i<videos.length;i++) {
//				if(i!=videos.length-1){
//					videoList.append("'"+videos[i].getName()+"',");
//				}else{
//					videoList.append("'"+videos[i].getName()+"'");
//				}
//			}
//			videoList.append("]");
//			System.out.println(videoList);
//		}else{
//			System.out.println("无视频文件!");
//		}
//	}
	
}
