package com.joburgess.fpzb;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.red5.logging.Red5LoggerFactory;
import org.red5.net.websocket.WebSocketConnection;
import org.red5.net.websocket.listener.WebSocketDataListener;
import org.red5.net.websocket.model.MessageType;
import org.red5.net.websocket.model.WSMessage;
import org.slf4j.Logger;

import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.Dao.impl.LiveStreamDaoImpl;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;

import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

public class WebSocketChatDataListener extends WebSocketDataListener implements Runnable{
	private static final Logger log = Red5LoggerFactory.getLogger(WebSocketChatDataListener.class, "Fpzb");
	private Set<WebSocketConnection> connections;

	public WebSocketChatDataListener() {
		setProtocol("Fpzb");

		this.connections = new HashSet();
	}

	public void onWSConnect(WebSocketConnection conn) {
		log.info("连接: {}", conn);
		if (conn.getHeaders().containsKey("Sec-WebSocket-Protocol")) {
			String protocol = (String) conn.getHeaders().get("Sec-WebSocket-Protocol");
			if (protocol.indexOf("Fpzb") != -1) {
				log.debug("Chat enabled");
			} else {
				log.info("Chat is not in the connections protocol list");
			}
		}
		this.connections.add(conn);
		new Thread(this).start();
	}

	public void onWSDisconnect(WebSocketConnection conn) {
		log.info("未连接: {}", conn);
		this.connections.remove(conn);
	}

	public void onWSMessage(WSMessage message) {
		if (!this.protocol.equals(message.getConnection().getProtocol())) {
			log.debug("Skipping message due to protocol mismatch");
			return;
		}
		if ((message.getMessageType() == MessageType.PING) || (message.getMessageType() == MessageType.PONG)) {
			return;
		}
		if (message.getMessageType() == MessageType.CLOSE) {
			message.getConnection().close();
			return;
		}
		String msg = new String(message.getPayload().array()).trim();
		log.info("onWSMessage: {}\n{}", msg, message.getConnection());
		if ((msg.indexOf('{') != -1) && (msg.indexOf(':') != -1)) {
			log.info("JSON encoded text message");

			JSONObject obj = null;
			JSONParser parser = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
			try {
				obj = (JSONObject) parser.parse(msg);
				log.debug("Parsed - keys: {}\ncontent: {}", obj.keySet(), obj);
				for (WebSocketConnection conn : this.connections) {
					try {
						conn.send(JSONValue.toJSONString(obj));
//						conn.send("Joburgess");
					} catch (UnsupportedEncodingException e) {
						
					}
				}
			} catch (ParseException e) {
				log.warn("Exception parsing JSON", e);
			}
		} else {
			log.info("Standard text message");
			for (WebSocketConnection conn : this.connections) {
				try {
					conn.send(msg);
				} catch (UnsupportedEncodingException e) {
					
				}
			}
		}
	}

	@Override
	public void run() {
		synchronized (protocol) {
			while(true){
				for (WebSocketConnection webSocketConnection : connections) {
					try {
						
						LiveStreamDao l=new LiveStreamDaoImpl();
						Page page=new Page();
						page.setSqlStartNumber(0);
						page.setOnePageNumber(12);
						List<LiveStream> ls=l.getLiving(page);
//						List<LiveStream> ls=l.getAllLive(page);
						if(ls.size()>0){
							StringBuffer lives=new StringBuffer("[");
							for (LiveStream liveStream : ls) {
								lives.append("{\"liveid\":\""+liveStream.getLiveStreamId()+"\",\"livename\":\""+liveStream.getLiveStreamName()+"\"},");
							}
							webSocketConnection.send(lives.substring(0, lives.length()-1).toString()+"]");
						}else{
							webSocketConnection.send("[]");
						}
						
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
