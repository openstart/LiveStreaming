package com.joburgess.bean;

public class LiveStreamInfo {

	private String liveName;
	private String liveState;
	public String getLiveName() {
		return liveName;
	}
	public void setLiveName(String liveName) {
		this.liveName = liveName;
	}
	public String getLiveState() {
		return liveState;
	}
	public void setLiveState(String liveState) {
		this.liveState = liveState;
	}
	
}
