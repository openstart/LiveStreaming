package com.joburgess.bean;

public class LiveStream {
	
	private String liveStreamName;
	private String liveStreamLocalFileName;
	private String liveStreamShotFileName;
	private String liveStreamPublicDate;
	private String liveStreamId;
	private String liveStreamState;
	private String canPlay;
	private String inRecycle;
	
	public String getLiveStreamName() {
		return liveStreamName;
	}
	public void setLiveStreamName(String liveStreamName) {
		this.liveStreamName = liveStreamName;
	}
	public String getLiveStreamLocalFileName() {
		return liveStreamLocalFileName;
	}
	public void setLiveStreamLocalFileName(String liveStreamLocalFileName) {
		this.liveStreamLocalFileName = liveStreamLocalFileName;
	}
	public String getLiveStreamShotFileName() {
		return liveStreamShotFileName;
	}
	public void setLiveStreamShotFileName(String liveStreamShotFileName) {
		this.liveStreamShotFileName = liveStreamShotFileName;
	}
	public String getLiveStreamPublicDate() {
		return liveStreamPublicDate;
	}
	public void setLiveStreamPublicDate(String liveStreamPublicDate) {
		this.liveStreamPublicDate = liveStreamPublicDate;
	}
	public String getLiveStreamId() {
		return liveStreamId;
	}
	public void setLiveStreamId(String liveStreamId) {
		this.liveStreamId = liveStreamId;
	}
	public String getLiveStreamState() {
		return liveStreamState;
	}
	public void setLiveStreamState(String liveStreamState) {
		this.liveStreamState = liveStreamState;
	}
	public String getCanPlay() {
		return canPlay;
	}
	public void setCanPlay(String canPlay) {
		this.canPlay = canPlay;
	}
	public String getInRecycle() {
		return inRecycle;
	}
	public void setInRecycle(String inRecycle) {
		this.inRecycle = inRecycle;
	}
	
}
