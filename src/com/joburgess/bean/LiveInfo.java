package com.joburgess.bean;

import java.io.Serializable;

public class LiveInfo implements Serializable{
	private int infoid;
	private String livetittle;
	private String livedescribe;
	private long duration;
	private int liveid;
	public int getInfoid() {
		return infoid;
	}
	public void setInfoid(int infoid) {
		this.infoid = infoid;
	}
	public String getLivetittle() {
		return livetittle;
	}
	public void setLivetittle(String livetittle) {
		this.livetittle = livetittle;
	}
	public String getLivedescribe() {
		return livedescribe;
	}
	public void setLivedescribe(String livedescribe) {
		this.livedescribe = livedescribe;
	}
	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public int getLiveid() {
		return liveid;
	}
	public void setLiveid(int liveid) {
		this.liveid = liveid;
	}
}
