package com.joburgess.bean;

import java.io.Serializable;

public class Page implements Serializable{

	private int numberOfPage;
	private int pageNumberOfNow;
	private int sqlStartNumber;
	private int onePageNumber;
	private String searchString;
	
	public int getNumberOfPage() {
		return numberOfPage;
	}
	public void setNumberOfPage(int numberOfPage) {
		this.numberOfPage = numberOfPage;
	}
	public int getPageNumberOfNow() {
		return pageNumberOfNow;
	}
	public void setPageNumberOfNow(int pageNumberOfNow) {
		this.pageNumberOfNow = pageNumberOfNow;
	}
	public int getSqlStartNumber() {
		return sqlStartNumber;
	}
	public void setSqlStartNumber(int sqlStartNumber) {
		this.sqlStartNumber = sqlStartNumber;
	}
	public int getOnePageNumber() {
		return onePageNumber;
	}
	public void setOnePageNumber(int onePageNumber) {
		this.onePageNumber = onePageNumber;
	}
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
}
