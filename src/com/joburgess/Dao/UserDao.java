package com.joburgess.Dao;

import java.util.List;

import com.joburgess.bean.User;

public interface UserDao {
	
	/**
	 * 监测用户登录信息
	 * @param user
	 * @return
	 */
	public List<User> checkUser(User user);

}
