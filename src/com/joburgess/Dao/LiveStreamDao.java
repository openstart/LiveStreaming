package com.joburgess.Dao;

import java.util.List;
import java.util.Map;

import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;

public interface LiveStreamDao {

	/**
	 * 添加直播
	 * @param liveStream
	 * @return
	 */
	public boolean addLiveStream(LiveStream liveStream);
	
	/**
	 * 根据记录id将该资源移动到回收站
	 * @param liveid
	 * @return
	 */
	public boolean updateinRecycleById(String[] liveid);
	
	/**
	 * 获取记录数据大小
	 * @return
	 */
	public int getLiveNumber(Map map);
	
	/**
	 * 恢复回收站资源
	 * @param liveid
	 * @return
	 */
	public boolean updateOutRecycleById(String[] liveid);
	
	/**
	 * 根据id查找直播记录
	 * @param liveid
	 * @return
	 */
	public LiveStream findLiveByid(int liveid);
	
	/**
	 * 找出回收站中所有资源
	 * @return
	 */
	public List<LiveStream> findLiveInRecycle(Page page);
	
	/**
	 * 根据id删除对应的直播记录
	 * @param liveid
	 * @return
	 */
	public boolean deleteLiveStreamById(String[] liveid);
	
	/**
	 * 根据直播信息查找出直播记录
	 * @param name
	 * @return
	 */
	public LiveStream getLiveStreamByStreamNameAndState(String name);
	
	/**
	 * 获取允许播放的数据
	 * @return
	 */
	public List<LiveStream> getAllowPlayStream();
	
	/**
	 * 获取正在进行中的直播
	 * @return
	 */
	public List<LiveStream> getLiving(Page page);
	
	/**
	 * 查询
	 * @param page
	 * @return
	 */
	public List<LiveStream> search(Page page);
	
	/**
	 * 获取历史直播记录
	 * @return
	 */
	public List<LiveStream> getHistroyLive(Page page);
	
	/**
	 * 获取所有数据信息
	 * @return
	 */
	public List<LiveStream> getAllLive(Page page);
	
	/**
	 * 更新直播状态
	 * @param name
	 * @return
	 */
	public boolean updateLiveState(String name);
	
	/**
	 * 更新直播状态为转换中
	 * @param name
	 * @return
	 */
	public boolean updateLiveStateToConverting(String name);
	
	/**
	 * 删除不符合限制的视频
	 * @param name
	 * @return
	 */
	public boolean deleteMinVideo(String name);
	
	/**
	 * 更改播放权限
	 * @param live
	 * @return
	 */
	public boolean updateCanPlay(LiveStream live);
	
	/**
	 * 监测是否允许播放
	 * @param live
	 * @return
	 */
	public boolean checkAllowPlay(LiveStream live);
	
	/**
	 * 动态查询直播记录
	 * @param live
	 * @return
	 */
	public List<LiveStream> getLiving(Map map);
}
