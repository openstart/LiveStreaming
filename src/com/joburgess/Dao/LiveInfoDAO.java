package com.joburgess.Dao;

import java.util.List;

import com.joburgess.bean.LiveInfo;

public interface LiveInfoDAO {

	/**
	 * 插入一条直播信息
	 * @param liveinfo
	 * @return
	 */
	public boolean addLiveInfo(LiveInfo liveinfo);
	
	/**
	 * 根据liveid查询对应记录
	 * @param liveid
	 * @return
	 */
	public LiveInfo findLiveInfoByLiveid(int liveid);
	
	/**
	 * 根据liveid修改相应的记录
	 * @param liveinfo
	 * @return
	 */
	public boolean updateLiveInfoByLiveid(LiveInfo liveinfo);
	
	/**
	 * 获取所有live信息
	 * @return
	 */
	public List<LiveInfo> getAllLiveInfo();
	
	/**
	 * 更具liveid删除对应记录
	 * @param liveid
	 * @return
	 */
	public boolean removeLiveinfoByLiveid(String[] liveid);
	
}
