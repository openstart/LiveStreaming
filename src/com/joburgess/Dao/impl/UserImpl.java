package com.joburgess.Dao.impl;

import java.io.Reader;
import java.util.List;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.joburgess.Dao.UserDao;
import com.joburgess.bean.User;

public class UserImpl implements UserDao {
	
	private static SqlMapClient sqlClient=null;
	
	static{
		try {
			sqlClient=SqlMapClientBuilder.buildSqlMapClient(Resources.getResourceAsReader("ibatis/config/sqlMapConfig.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<User> checkUser(User user) {
		try {
			return sqlClient.queryForList("checkUser",user);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
