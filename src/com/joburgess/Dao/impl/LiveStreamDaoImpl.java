package com.joburgess.Dao.impl;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.joburgess.Dao.LiveStreamDao;
import com.joburgess.bean.LiveStream;
import com.joburgess.bean.Page;

public class LiveStreamDaoImpl implements LiveStreamDao{
	
	private static SqlMapClient sqlMapClient=null;
	
	static{
		try {
			Reader reader=Resources.getResourceAsReader("ibatis/config/sqlMapConfig.xml");
			sqlMapClient=SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean addLiveStream(LiveStream liveStream) {
		Object object=null;
		try {
			object=sqlMapClient.insert("addLiveStream",liveStream);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Override
	public LiveStream getLiveStreamByStreamNameAndState(String name) {
		
		try {
			return (LiveStream) sqlMapClient.queryForList("getLiveStreamByStreamNameAndState",name).get(0);
		} catch (Exception e) {
			return null;
		}
		
	}

	@Override
	public boolean updateLiveState(String name) {
		try {
			sqlMapClient.update("updateLiveState",name);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return false;
	}

	@Override
	public List<LiveStream> getLiving(Page page) {
		try {
			return sqlMapClient.queryForList("getLiving",page);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<LiveStream> getAllLive(Page page) {
		try {
			return sqlMapClient.queryForList("getAllLive",page);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<LiveStream> getAllowPlayStream() {
		try {
			return sqlMapClient.queryForList("getAllowPlay");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean updateCanPlay(LiveStream live) {
		try {
			return sqlMapClient.update("updateLivePermission",live)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean checkAllowPlay(LiveStream live) {
		try {
			return sqlMapClient.queryForList("checkAllowPlay",live).size()==1?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteLiveStreamById(String[] liveid) {
		try {
			return sqlMapClient.delete("deleteLiveById", liveid)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<LiveStream> getHistroyLive(Page page) {
		try {
			return sqlMapClient.queryForList("getHistoryLiving",page);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean updateinRecycleById(String[] liveid) {
		try {
			return sqlMapClient.update("updateInrecycle",liveid)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public LiveStream findLiveByid(int liveid) {
		try {
			return (LiveStream) (sqlMapClient.queryForList("findLiveByid", liveid).size()>0?sqlMapClient.queryForList("findLiveByid", liveid).get(0):null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<LiveStream> findLiveInRecycle(Page page) {
		try {
			return sqlMapClient.queryForList("findLiveInRecycle",page);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean updateOutRecycleById(String[] liveid) {
		try {
			return sqlMapClient.update("updateOutrecycle",liveid)>0?true:false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateLiveStateToConverting(String name) {
		try {
			return sqlMapClient.update("updateLiveStateToConverting",name)>0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteMinVideo(String name) {
		try {
			return sqlMapClient.delete("deleteminvideo", name)>0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public int getLiveNumber(Map map) {
		try {
			return (int) sqlMapClient.queryForList("getLiveNumber",map).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<LiveStream> search(Page page) {
		try {
			return sqlMapClient.queryForList("Serach",page);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<LiveStream> getLiving(Map map) {
		try{
			return sqlMapClient.queryForList("findLiving", map);
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static void main(String[] args) {
//		LiveStreamDaoImpl lsd=new LiveStreamDaoImpl();
//		Page p=new Page();
//		p.setSqlStartNumber(0);
//		p.setOnePageNumber(12);
//		p.setSearchString("体质监测");
//		System.out.println(lsd.search(p));
//		
//		String[] i=new String[]{"41"};
//		System.out.println(lsd.updateinRecycleById(i));
//		
	}
	
}
