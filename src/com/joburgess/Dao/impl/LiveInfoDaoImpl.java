package com.joburgess.Dao.impl;

import java.util.List;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;
import com.joburgess.Dao.LiveInfoDAO;
import com.joburgess.bean.LiveInfo;
import com.joburgess.bean.LiveStream;

public class LiveInfoDaoImpl implements LiveInfoDAO {
	
	private static SqlMapClient smc=null;
	
	static{
		try {
			smc=SqlMapClientBuilder.buildSqlMapClient(Resources.getResourceAsReader("ibatis/config/sqlMapConfig.xml"));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public boolean addLiveInfo(LiveInfo liveinfo) {
		try {
			smc.insert("addLiveInfo", liveinfo);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public LiveInfo findLiveInfoByLiveid(int liveid) {
		try {
			@SuppressWarnings("unchecked")
			List<LiveInfo> lis= smc.queryForList("findLiveInfoByLiveid", liveid);
			return lis.size()>0?lis.get(0):null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean updateLiveInfoByLiveid(LiveInfo liveinfo) {
		try {
			return smc.update("updateLiveInfoByLiveid",liveinfo)>0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LiveInfo> getAllLiveInfo() {
		try {
			return smc.queryForList("getAllLiveInfo");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<LiveInfo> search(LiveInfo l){
		try {
			return smc.queryForList("serach",l);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean removeLiveinfoByLiveid(String[] liveid) {
		try {
			return smc.delete("removeLiveInfoByliveid", liveid)>0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
//	public static void main(String[] args) {
//		LiveInfoDaoImpl ldi=new LiveInfoDaoImpl();
//		LiveInfo li=new LiveInfo();
//		li.setLivetittle("Joburgess");
//		li.setLivedescribe("武汉飞鹏数码有限责任公司成立于2004年4月");
//		li.setDuration(0);
//		li.setLiveid(63);
//		System.out.println(ldi.search(li).size());
//	}
}
