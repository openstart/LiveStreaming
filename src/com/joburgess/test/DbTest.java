package com.joburgess.test;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;

public class DbTest {
	
	public static void main(String[] args) {
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://192.168.1.39:3306/fpzb", "root", "tiger");
			PreparedStatement sta=con.prepareStatement("select * from livestream");
			ResultSet rs=sta.executeQuery();
			while(rs.next()){
				
				Date date=rs.getDate("liveStreamPublicDate");
				System.out.println(rs.getString("liveStreamPublicDate"));
				System.out.println(new SimpleDateFormat("yyyy:MM:dd hh:mm:ss").format(date));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
