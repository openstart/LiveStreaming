package com.joburgess.test;

import java.util.Scanner;

public class PrintCircle {

	private static double r;

	private static double x;

	PrintCircle(double inr) {

		r = inr;

		x = 0;

	}

	public static void print() {

		double y = 0;

		for (int i = 5; i >= -5; i--) {

			if (i >= 0) {

				y = r - i * r / 5;

				System.out.print(PrintCircle.leftSpace(r - y) + "*");

				System.out.println(PrintCircle.midSpace() + "*");

				for (int j = 0; j < (int) r / 20; j++) {

					System.out.println();

				}

			} else {

				y = r + i * r / 5;

				System.out.print(PrintCircle.leftSpace(r - y) + "*");

				System.out.println(PrintCircle.midSpace() + "*");

				for (int j = 0; j < (int) r / 20; j++) {

					System.out.println();

				}

			}

		}

	}

	public static String leftSpace(double y) {

		x = Math.sqrt(r * r - y * y);

		String s = "";

		for (int j = 0; j < r - x; j++) {

			s += " ";

		}

		return s;

	}

	public static String midSpace() {

		String s = "";

		for (int j = 0; j <= 2 * x; j++) {

			s += " ";

		}

		return s;

	}

	@SuppressWarnings("static-access")
	public static void main(String[] ages) {

		System.out.println("input半径：");

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		double cr = sc.nextFloat();

		PrintCircle circle = new PrintCircle(cr);

		circle.print();
		

		System.out.println(r);

	}
	
}