<%@page import="java.net.InetAddress"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
	String path=request.getContextPath()+"";
	pageContext.setAttribute("path", path);
	pageContext.setAttribute("ip",InetAddress.getLocalHost().getHostAddress());
%>

<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>飞鹏直播</title>
	<link rel="stylesheet" href="${path}/playerlist/css/sweetalert.css" />
	<link href="${path}/playerlist/css/jquery.brickfolio.min2.css" rel="stylesheet">
	<link rel="stylesheet" href="${path }/playerlist/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="${path}/playerlist/css/normalize.css" />
    <link href="${path}/playerlist/css/Pager.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${path}/playerlist/css/lc_switch.css" />
	<script src="http://libs.useso.com/js/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="${path }/playerlist/js/bootstrap.js"></script>
	<script type="text/javascript" src="${path}/playerlist/js/swfobject.js"></script>
	<!--[if IE]>
		<script src="http://libs.useso.com/js/html5shiv/3.7/html5shiv.min.js"></script>
	<![endif]-->
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	    	 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">飞鹏</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">飞鹏直播</a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav control">
	        <li class="${param.state<1?'active':'否' }"><a href="${path }/live?state=0">全部</a></li>
	        <li class="${param.state==1?'active':'否' }"><a href="${path }/live?state=1">直播</a></li>
	        <li  class="${param.state==2?'active':'否' }"><a href="${path }/live?state=2">历史</a></li>
			<c:if test="${sessionScope.user.role==0 }">
				<li class="${param.state==3?'active':'否' }"><a class="" href="${path }/live?state=3">回收站</a></li>
			</c:if>
	      </ul>
		  <c:if test="${sessionScope.user.role==0 }">
		      <form action="/Fpzb/live?state=4" method="post" class="navbar-form navbar-left" role="search">
		        <div class="form-group" >
		          <input type="text" class="form-control" name="search" placeholder="查询" >
		        </div>
		        <button type="submit" class="btn btn-default">查询</button>
		      </form>
		  </c:if>
	      <ul class="nav navbar-nav navbar-right">
			<c:if test="${sessionScope.user!=null }">
				<li><a>${sessionScope.user.username}</a></li>
				<li><a href="${path }/lo">退出</a></li>
			</c:if>
			<c:if test="${sessionScope.user==null }">
<%-- 				<li><a href="${path }/login">登陆</a></li> --%>
				<li><a data-toggle="modal" data-target="#login" style="cursor: pointer;">登陆</a></li>
			</c:if>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div class="htmleaf-container" style="height:100%;">
	  <div class="demo-wrapper" style="height:100%;">
			<div class="brickfolio" style="height:100%;">
				<!-- 非直播页面 -->
				<c:if test="${param.state!=1||sessionScope.user.role==0 }">
					<c:forEach var="live" items="${requestScope.lives }">
						<div class="bf-item showCard bf-item${live.liveStreamState }" id="showCard">
							<c:choose>
								<c:when test="${param.state==1 }">
									<embed src="${path }/playerlist/ckplayer/ckplayer2.swf"
										 flashvars="f=rtmp://${ip }:1935/Fpzb/${live.liveStreamName }&c=1&lv=1&b=1&p=1" width="100%" height="200px" align="middle" 
										 allowScriptAccess="always" allowFullscreen="true" type="application/x-shockwave-flash"></embed>
								</c:when>
								<c:otherwise>
									<img src="${path}/screenshot/${live.liveStreamShotFileName }.jpg" alt="${live.liveStreamName }">
				                   	<c:choose>
				                   		<c:when test="${live.liveStreamState!=2 }">
				                   			<span data-toggle="modal" data-target="#videoPlay" class="pc" title="${live.liveStreamName }" state="${live.liveStreamState }" fileName="${live.liveStreamLocalFileName }" liveid="${live.liveStreamId }">
												<b></b>
				                   			</span>
				                   		</c:when>
				                   		<c:otherwise>
				                   			<span class="convert" title="${live.liveStreamName }" state="${live.liveStreamState }" fileName="${live.liveStreamLocalFileName }" liveid="${live.liveStreamId }">
				                   				<b style="position:absolute;top:80px;left:75px;color:white;">视频转换中...</b>
				                   			</span>
				                   		</c:otherwise>
				                   	</c:choose>
								</c:otherwise>
							</c:choose>
						  <h4 class="state${live.liveStreamState }" title="${live.liveStreamShotFileName }">
						  	<span class="lv">
						  		<label class="pcz" data-toggle="tooltip" data-placement="bottom" title="单击选中，双击全选/全不选!">
						  			<c:if test="${sessionScope.user.role==0 }">
								  	<input id="${live.liveStreamId }" type="checkbox" value="${live.liveStreamId }" name="${live.liveStreamId }" style="display: none;"/>
								  	</c:if>
								  	<span onselectstart="return false;" style="-moz-user-select:none;">${live.liveStreamName }</span>
						  		</label>
						  	</span>
							  	<c:if test="${sessionScope.user.role==0 }">
						  			<a id="${live.liveStreamId }" title="回收" class="deletelive glyphicon glyphicon-trash ${live.liveStreamState!=0?'':'disabled' }"></a>
						  			<c:if test="${live.inRecycle==0&&live.liveStreamState!=0 }">
						  				<a img="${path}/screenshot/${live.liveStreamShotFileName }.jpg" liveid="${live.liveStreamId }" class="glyphicon glyphicon-wrench" data-toggle="modal" data-target="#EditInfo"></a>
						  			</c:if>
						  			<c:if test="${live.inRecycle==1 }">
						  				<a id="${live.liveStreamId }" title="恢复" class="restorelive">
						  					<span class="glyphicon glyphicon-repeat"></span>
						  				</a>
						  			</c:if>
						  			<c:if test="${live.inRecycle!=1 }">
						  				<input title="是否允许播放" type="checkbox" name="live" class='lcs_check' autocomplete="${live.canPlay==1?'on':'off' }" ${live.canPlay==1?"checked='checked'":'' } value="${live.liveStreamName }" fileName="${live.liveStreamLocalFileName }" liveid="${live.liveStreamId }" />
						  			</c:if>
						  		</c:if>
						  </h4>
						  	
						  	<c:forEach var="liveinfo" items="${requestScope.liveinfos }">
						  		<c:if test="${liveinfo.liveid==live.liveStreamId }">
					  				<p>
					  					<span id="livetittle" class="lssf">${liveinfo.livetittle }</span>
					  					<span id="livedescribe">${liveinfo.livedescribe }</span>
					  				</p>
						  		</c:if>
						  	</c:forEach>
							
					  	</div>
						<script type="text/javascript">
							$('[data-toggle="tooltip"]').tooltip();
						</script>
					</c:forEach>
				</c:if>
				
				<!-- 直播页面 -->
				<c:if test="${param.state==1&&sessionScope.user.role!=0 }">
					<div class="container playerwindow">
						<c:if test="${fn:length(requestScope.lives)==1 }">
							<c:forEach var="live" items="${requestScope.lives }">
								<div class="row" style="height:100%;">
								  <div class="col-md-12" style="height:100%;">
								  	<span class="videoName">${live.liveStreamName }</span>
									<embed src="${path }/playerlist/ckplayer/ckplayer.swf"
									 flashvars="f=rtmp://${ip }:1935/Fpzb/${live.liveStreamName }&c=1&lv=1&b=1&p=1" width="100%" height="100%" align="middle" 
									 allowScriptAccess="always" allowFullscreen="true" type="application/x-shockwave-flash" WindowlessVideo="1" style="z-index: -1;" 
									 wmode="opaque" ></embed>
								  </div>
								</div>
							</c:forEach>
						</c:if>
						<c:if test="${fn:length(requestScope.lives)==2 }">
							<div class="row" style="height:100%;">
								<c:forEach var="live" items="${requestScope.lives }">
								  <div class="col-md-6" style="height:100%;">
								  	<span class="videoName">${live.liveStreamName }</span>
									<embed src="${path }/playerlist/ckplayer/ckplayer.swf"
									 flashvars="f=rtmp://${ip }:1935/Fpzb/${live.liveStreamName }&c=1&lv=1&b=1&p=1" width="100%" height="100%" align="middle" 
									 allowScriptAccess="always" allowFullscreen="true" type="application/x-shockwave-flash"  wmode="opaque"></embed>
								  </div>
								</c:forEach>
							</div>
						</c:if>
						<c:if test="${fn:length(requestScope.lives)>2 }">
							<div class="row" style="height:100%;">
								<c:forEach var="live" items="${requestScope.lives }" varStatus="util" begin="0" end="4">
									  <div class="col-md-6">
								  		<span class="videoName">${live.liveStreamName }</span>
										<embed src="${path }/playerlist/ckplayer/ckplayer.swf"
										 flashvars="f=rtmp://${ip }:1935/Fpzb/${live.liveStreamName }&c=1&lv=1&b=1&p=1" width="100%" height="450px" align="middle" 
										 allowScriptAccess="always" allowFullscreen="true" type="application/x-shockwave-flash"  wmode="opaque"></embed>
									  </div>
								</c:forEach>
							</div>
						</c:if>
						
					</div>
				</c:if>
				<div id="livenum" livenum='0' page="" role="${sessionScope.user.role }" ip="${ip }" style="display:none;"></div>
		  	</div>
		  	<!-- 分页 -->
		  <c:if test="${requestScope.pageinfo.numberOfPage>1 }">
			  <div class="row" style="width:100%;">
			  	<div class="col-md-offset-10 col-xs-offset-6">
			  		 <nav>
					  <ul class="pagination">
					    <li>
					      <a href="${path }/live?state=${param.state }&pagenow=${requestScope.pageinfo.pageNumberOfNow==1?1:requestScope.pageinfo.pageNumberOfNow-1 }" aria-label="Previous">
					        <span aria-hidden="true">&laquo;</span>
					      </a>
					    </li>
						<c:forEach var="pagesize" begin="1" end="${requestScope.pageinfo.numberOfPage}">
							<c:choose>
								<c:when test="${requestScope.pageinfo.pageNumberOfNow==pagesize }">
					    			<li class="active"><a href="${path }/live?state=${param.state }&pagenow=${pagesize }">${pagesize }</a></li>
					    		</c:when>
					    		<c:otherwise>
					    			<li class=""><a href="${path }/live?state=${param.state }&pagenow=${pagesize }">${pagesize }</a></li>
					    		</c:otherwise>
							</c:choose>
						</c:forEach>
					    <li>
					      <a href="${path }/live?state=${param.state }&pagenow=${requestScope.pageinfo.pageNumberOfNow==requestScope.pageinfo.numberOfPage?requestScope.pageinfo.numberOfPage:requestScope.pageinfo.pageNumberOfNow+1 }" aria-label="Next">
					        <span aria-hidden="true">&raquo;</span>
					      </a>
					    </li>
					  </ul>
					</nav>
			  	</div>
			  </div>
		  </c:if>
		</div>
<!-- 	  <div id="pager" ></div> -->
</div>

	<!-- 登陆 -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> 登陆</h4>
	      </div>
	        <form action="/Fpzb/login" id="dlf" method="post" class="form-horizontal">
		      <div class="modal-body">
				  <div class="form-group">
				    <label for="inputEmail3" class="control-label">用户名:</label>
				      <input type="text" class="form-control" name="username" placeholder="用户名">
				  </div>
				  <div class="form-group">
				    <label for="inputPassword3" class="control-label">密码:</label>
				      <input type="password" class="form-control" name="password" placeholder="密码">
				  </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
<!-- 		        <button type="submit" class="btn btn-primary">登陆</button> -->
		        <button type="button" id="dl" class="btn btn-primary">登陆</button>
		      </div>
			</form>
	    </div>
	  </div>
	</div>
	
	<!-- 信息修改 -->
	<div class="modal fade" id="EditInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel"><span class="glyphicon glyphicon-pencil"></span> 修改信息</h4>
	      </div>
	        <form>
		      <div class="modal-body">
		          <div class="form-group">
			      	  <div class="col-xs-12 col-md-12" style="padding:0;">
					    <a href="#" class="thumbnail">
					      <img src="" alt="截图" style="max-height:250px;">
					    </a>
					  </div>
				  </div>
		          <div class="form-group">
		            <label for="recipient-name" class="control-label">标题:</label>
		            <input type="text" name="livetittle" class="form-control" id="recipient-name">
		          </div>
		          <div class="form-group">
		            <label for="message-text" class="control-label">内容:</label>
		            <textarea class="form-control" name="livedescribe" id="message-text" style="max-width: 100%"></textarea>
		          </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
		        <button type="button" id="editinfobutton" class="btn btn-primary">确定</button>
		      </div>
	        </form>
	    </div>
	  </div>
	</div>
	
	<!-- 播放器 -->
	<div class="modal fade bs-example-modal-lg" id="videoPlay" tabindex="-1" role="dialog" style="padding:0;">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content center-block">
<!-- 	      <video id="vp" class="vp" style="max-width: 100%;max-height: auto;width:800px;" autoplay="autoplay" controls="controls"></video> -->
	    </div>
	  </div>
	</div>

<!--     <div class="player"  onclick='removePlayer()'></div> -->
	
	<script src="${path}/playerlist/js/jquery.brickfolio.js"></script>
	<script type="text/javascript" src="${path}/playerlist/js/addPlayer.js"></script>
	<script src="${path}/playerlist/js/lc_switch.js" type="text/javascript"></script>
	<script src="${path}/playerlist/js/sweetalert.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${path }/playerlist/ckplayer/ckplayer.js" charset="utf-8"></script>
	<script>
	
		jQuery(function($){
			var llu;
			
			$('.pcz input').attr('checked',false);//初始化单选框状态
			
// 			if(getUrlParam("state")==1&&$('#livenum').attr('role')!=''&&$('#livenum').attr('role')==0){
// 				joburgess();
// 			}
			
			if(getUrlParam("state")==1){
				llu=setInterval("listenLivingUpdate()",1000);
			}else{
				clearInterval(llu);
			}
			
			var $container = $('.brickfolio'),
				$load_delay = $('#load_delay'),
				$animation = $(''),
				$mixed_delay = $('#mixed_delay'),
				getAnimation = function(){
					return 'flip' + ($mixed_delay.is(':checked') ? ' ' + $mixed_delay.val() : '');
				},
				getLoadTime = function(){
					return $load_delay.is(':checked') ? $load_delay.val() : 0;
				};

			$animation.add($mixed_delay).add($load_delay).on('change', function(){
				$container.brickfolio({animation: getAnimation(), loadTime: getLoadTime()});
			});
			$container.brickfolio({animation: getAnimation(), loadTime: getLoadTime()});
			
			$('.lcs_check').lc_switch();
			$('.lcs_check').change(function(){
				alert($(this).attr('value'));
			});
		});
	</script>
</body>
</html>