var so = new SWFObject("playerlist/player.swf","FPplayer","600","400","9","#000000");
var ip=$('#livenum').attr('ip');

function addPlayer(liveName,state,filename){
	
	var flashvars={f:'rtmp://'+ip+':1935/Fpzb/'+liveName,c:1,b:1,lv:'1'};
	
	var $divplayer="<div class='play' id='play'></div>";
	var $divplayerbyhtml5="<video src='http://"+ip+":5080/Fpzb/videos/"+
	filename+
//	var $divplayerbyhtml5="<video src='http://"+ip+":5080/Fpzb/videostream?video="+
//	filename+
	".mp4' autoplay='autoplay' controls='controls' class='center-block' id='video' style='max-width: 100%;max-height: 500px;box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);border-radius: 5px;'></video>";
	
	if(state==0){
		$('#videoPlay .modal-content').append($divplayer);
		var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};
		CKobject.embedSWF('/Fpzb/playerlist/ckplayer/ckplayer.swf','play','ckplayer_play','800','450',flashvars,params);
	}else{
		$('#videoPlay .modal-content').append($divplayerbyhtml5);
	}
	
	$('video').bind('contextmenu',function() { return false; });//屏蔽右键菜单
}

function removePlayer(){
	$("#play").remove();
	$(".player").fadeOut(300);
}

$('#videoPlay').on('hide.bs.modal', function (e) {
	$('#videoPlay video').attr('src','');
	$("#play").remove();
	$("#videoPlay video").remove();
});
$('#videoPlay').on('show.bs.modal',function(e){
	var param = $(e.relatedTarget);
	var title=param.attr('title');
	var state=param.attr('state');
	var fileName=param.attr('fileName');
	$.post('/Fpzb/ccp',{liveid:param.attr('liveid')},function(data){
		if(data=="true"){
			addPlayer(title,state,fileName);
		}else if(data=="false"){
			swal("","您无权播放该视频!","warning");
			$('#videoPlay').modal('toggle');
		}else{
			swal("正在处理...");
			$('#videoPlay').modal('toggle');
		}
	});
});

$('#dl').click(function(){
	$.ajax({
		type:"POST",
		url:"/Fpzb/login",
		data:$('#dlf').serialize(),
		feforeSend:function(){
			swal({
				title:"",
				text:"登陆中...",
				type:"info",
				showConfirmButton:false,
			});
		},
		success:function(data){
			if(data.trim()=="yes"){
				swal({
					title:"",
					text:"跳转中...",
					type:"success",
					timer:"500",
					showConfirmButton:false
				},
				function(){
					location.reload();
					location.replace(location);
				});
			}else{
				swal({
					title:"",
					text:"登陆失败，请重试！",
					type:"error",
					timer:"1000",
					showConfirmButton:false,
				});
			}
		}
	});
});

$('.deletelive').click(function(){
	var pcs=$('.pcz :checkbox');
	var liveids="";
	var t=0;
	for(var i=0;i<pcs.length;i++){
		if(pcs[i].checked){
			liveids+=pcs[i].value+",";
			t++;
		}
	}
	if(liveids.length==0){
		liveids=$(this).attr('id')+",";
		t=1
	}
	
	swal({
		  title: "",
		  text: "您确认将这"+t+"条数据"+(getUrlParam("state")==3?"删除？此操作无法回退！":"移动到回收站？"),
		  type: (getUrlParam("state")==3?"error":"warning"),
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  allowOutsideClick:true,
		  closeOnConfirm: true,
		  showLoaderOnConfirm: true
		},
		function(){
			$.post('/Fpzb/dl',
					{
						liveid:liveids.substring(0,liveids.length-1),
						control:"delete"
					},
					function(data){
						location.reload();
						location.replace(location);
					}
			);
		});
});

$('.restorelive').click(function(){
	var pcs=$('.pcz :checkbox');
	var liveids="";
	var t=0;
	for(var i=0;i<pcs.length;i++){
		if(pcs[i].checked){
			liveids+=pcs[i].value+",";
			t++;
		}
	}
	if(liveids.length==0){
		liveids=$(this).attr('id')+",";
		t=1;
	}

	swal({
		  title: "",
		  text: "已选中"+t+"条数据，是否恢复？",
		  type: "info",
		  showCancelButton: true,
		  confirmButtonColor: "#80FF92",
		  allowOutsideClick:true,
		  closeOnConfirm: true,
		  showLoaderOnConfirm: true
		},
		function(){
			$.post('/Fpzb/dl',
				{liveid:liveids.substring(0,liveids.length-1),
					control:"restore"
				},
				function(data){
					location.reload();
				});
		});
});

$('.play a').click(function(event){
	removePlayer();
	event.stopPropagation();
});

/**
 * 全选/全不选
 */
$('.pcz').dblclick(function(){
	$("input[type=checkbox]:first").each(function(){
		if(this.checked==false){
			$("input[type=checkbox]").each(function(){
				this.checked=true;
			});
			return false;
		}else{
			$("input[type=checkbox]").each(function(){
				this.checked=false;
			});
			return false;
		}
	});
});

function thisMovie(movieName) {
    if (navigator.appName.indexOf("Microsoft") != -1) {
        return window[movieName]
    }else {
        return document[movieName]
    }
}

function postAct(player,pars){
	thisMovie(player).sendtxt(""+pars+"");
}

function myBrowser(){
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (userAgent.indexOf("Opera") > -1) {
        return "Opera"
    }; //判断是否Opera浏览器
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    } //判断是否Firefox浏览器
    if (userAgent.indexOf("Chrome") > -1){
    	return "Chrome";//判断是否Chrome浏览器
    }
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    } //判断是否Safari浏览器
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    }; //判断是否IE浏览器
}

$('.control li').click(function(){
	var conli=$('.control').children();
	$(this).attr('class','active');
	for(var i=0;i<conli.length;i++){
		alert(conli[i].text());
		if(conli[i].text()!=$(this).text()){
			conli[i].removeClass('active');
		}
	}
});

$('#EditInfo').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	$('.modal-body img').attr('src',button.attr('img'));
	$('#EditInfo #recipient-name').val(button.parent().parent().find('#livetittle').text());
	$('#EditInfo #message-text').val(button.parent().parent().find('#livedescribe').text());
	$('#editinfobutton').attr('liveid',button.attr('liveid'));
});


$('#editinfobutton').click(function(){
	var livetittle=$('#EditInfo #recipient-name').val();
	var livedescribe=$('#EditInfo #message-text').val();
	var liveid=$('#editinfobutton').attr('liveid');
	
	$.post('/Fpzb/uli',
			{
				livetittle:livetittle,
				livedescribe:livedescribe,
				liveid:liveid
			},
			function(data){
				if(data=="operate success"){
					$('#EditInfo').modal('toggle');
				}else{
					swal("","操作失败，请稍后重试！","error")
				}
			});
	
});


//地址栏参数
function getUrlParam(name){
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg);  //匹配目标参数
	if (r!=null) return unescape(r[2]); return null; //返回参数值
}

//建立websocket连接
function joburgess(){
	var ws = new WebSocket("ws://"+ip+":8081/Fpzb","Fpzb");
	ws.onopen = function(){
		$('#livenum').attr('livenum',$('.bf-item0').length);
	};
	ws.onmessage = function(message){
		var ls=$.parseJSON(message.data);
		console.log(ls.length);
		var livenum=$('#livenum').attr('livenum');
		if(livenum!=ls.length&&getUrlParam("state")==1&&ls.length!=0){
			swal({
				  title: "",
				  text: "直播记录已发生改变，是否刷新页面数据？",
				  type: "info",
				  showCancelButton: true,
				  confirmButtonColor: "#81F5FF",
				  allowOutsideClick:true,
				  closeOnConfirm: true,
				  showLoaderOnConfirm: true
				},
				function(){
					location.reload();
				});
			console.log(ls.length+" "+livenum+" "+$('.bf-item0').length);
//			$('#livenum').attr('livenum',$('.bf-item0').length);
			$('#livenum').attr('livenum',ls.length);
		}else if(ls.length==0&&livenum!=ls.length){
			location.reload();
			$('#livenum').attr('livenum',0);
		}
	};
	function postToServer(){
		ws.send(document.getElementById("msg").value);
		document.getElementById("msg").value = "";
	}
	function closeConnect(){
		ws.close();
	}
}

function listenLivingUpdate(){
//	imgLoad();
	$.post(
		'/Fpzb/clu',
		{state:'1'},
		function(data){
			var ls=data.trim();
			var page=$('.bf-item0').length;
			var flash=$('embed').length;
			var role=$('#livenum').attr('role');
			if(role=='0'){
				if(ls!=page){
					location.reload();
					location.replace(location);
				}
			}else{
				if(flash!=ls&&getUrlParam("state")==1){
					location.reload();
					location.replace(location);
				}
			}
		}
	);
}

function imgLoad() {
		console.log($('.brickfolio #showCard img:last').attr("src"));
//		$('.brickfolio #showCard img:last').each(function(){
//			var img=$(this);
//			var imgsrc=$(this).attr('src');
//			img.error(function(){
//				console.log("JOburgess");
//			});
//			$.ajax({
//				type:"POST",
//				url:imgsrc,
//				success:function(data){
//					$(this).attr('src',imgsrc+"?m="+Math.random());
//				},
//				error:function(a,b,c){
//					console.log(a.status+" "+b);
//				}
//			});
//		 });
}

//$('.pc').mouseover(function(){
//	$('.showCard').addClass('gallery');
//	$(this).parent().removeClass('gallery');
////	$("#showCard").addClass('gallery');
////	$(this).prev("img").removeClass('gallery');
//});
//
//$('.pc').mouseout(function(){
//	$('.showCard').removeClass('gallery');
////	$(this).parent().removeClass('gallery');
////	$(this).prev("img").removeClass('gallery');
//	
//});